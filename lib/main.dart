import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'file:///C:/Users/Mahmoud/Desktop/QueueOver/queue/lib/ui/screens/sign_in.dart';
import 'file:///C:/Users/Mahmoud/Desktop/QueueOver/queue/lib/ui/screens/splash.dart';
import 'package:queue/utils/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  StateClass createState() => StateClass();
}

class StateClass extends State<MyApp> {
  bool isEn = true;

  @override
  void initState() {
    super.initState();
    _language();
  }

  Future<void> _language() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String language = prefs.getString("lang");
    if (language == null) {
      isEn = true;
    } else {
      if (language == "ar") isEn = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (BuildContext context, Widget child) {
        return Directionality(
          textDirection: isEn ? TextDirection.ltr : TextDirection.rtl,
          child: Builder(
            builder: (BuildContext context) {
              return MediaQuery(
                data: MediaQuery.of(context).copyWith(
                  textScaleFactor: 1.0,
                ),
                child: child,
              );
            },
          ),
        );
      },
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      supportedLocales: [Locale('ar'), Locale('en')],
      localeResolutionCallback: (locale, supportedLocales) {
        // Check if the current device locale is supported
        for (var supportedLocale in supportedLocales) {
          if (supportedLocale.languageCode == locale.languageCode &&
              supportedLocale.countryCode == locale.countryCode) {
            return supportedLocale;
          }
        }
        // If the locale of the device is not supported, use the first one
        // from the list (English, in this case).
        return supportedLocales.first;
      },
      theme: ThemeData(primarySwatch: Colors.green, fontFamily: 'arbfonts'),
      home: SplashScreen(),
      // routes: {
      //   "Splash": (context) => SplashScreen(),
      //   "Login": (context) => SignInScreen(),
      //   "Forget": (context) => SplashScreen(),
      //   "Register": (context) => SplashScreen(),
      //   'Home': (context) => SplashScreen(),
      //   'Cart': (context) => SplashScreen(),
      //   'Brands': (context) => SplashScreen(),
      //   'Categories': (context) => SplashScreen(),
      //   'Items': (context) => SplashScreen(),
      //   'Details': (context) => SplashScreen(),
      //   'Reviews': (context) => SplashScreen(),
      //   'OrderDetails': (context) => SplashScreen(),
        // ClientHomeContainer.routName: (context) => ClientHomeContainer(),
        // SurveyScreen.routeName: (context) => SurveyScreen(),
      // },
    );
  }
}
