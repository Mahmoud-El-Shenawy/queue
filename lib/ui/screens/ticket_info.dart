import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:kf_drawer/kf_drawer.dart';
import 'package:lottie/lottie.dart';
import 'package:queue/ui/screens/update_time.dart';
import 'package:queue/utils/app_localizations.dart';
import 'package:queue/utils/sharedPrefrence.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:queue/style/theme.dart' as Theme;
import '../../utils/global.dart';
import '../../main.dart';
import 'package:queue/ui/widgets/for_open_map.dart';

class TicketInfo extends KFDrawerContent {
  int myId;

  TicketInfo(this.myId);

  @override
  _TicketInfoState createState() => _TicketInfoState(this.myId);
}

class _TicketInfoState extends State<TicketInfo> with TickerProviderStateMixin {
  int myId;

  _TicketInfoState(this.myId);

  bool loading = true;
  int _current = 0;
  int _currentStep = 10;
  int _retainTime = 0; // Seconds
  double m = 0;
  int mn = 0;

  @override
  void initState() {
    super.initState();
    homeAPI();
  }

  List categoriesList = new List();

  void calculateTime(String date, String time) {
    // Some process here
    DateTime now = DateTime.now();
    DateTime dob = DateTime.parse(date + " " + time);
    final difference = now.difference(dob).inSeconds;
    print(now.toString());
    print("Aya");
    print(dob);
    print(difference);
    if (difference == 0) {
      setState(() {
        _currentStep = 0;
        _retainTime = 0;
      });
    } else {
      setState(() {
        _retainTime = difference;
      });
    }
    generateStep();
  }

  void generateStep() {
    m = (_retainTime / 10);
    mn = 0;
    // add counter here & plus by 1 mn & check in counter if mn == m
  }

  double getNumber(double input, {int precision = 2}) => double.parse(
      '$input'.substring(0, '$input'.indexOf('.') + precision + 1));
  Timer _timer;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        mn = mn + 1;
        print("Count");
        m = getNumber(m, precision: 0); // 113.9
        print(mn);
        print(m);
        if (mn == m) {
          setState(() {
            _currentStep = _currentStep - 1;
            _retainTime = _retainTime - mn;
            print("Minus 1");
            mn = 0;
          });
        } else if (_retainTime == 0 || _retainTime < 0 || mn > m) {
          setState(() {
            timer.cancel();
          });
        } else {
          // not do anything
        }
      },
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  void homeAPI() async {
    setState(() {
      loading = true;
    });

    String token = await getToken();

    http.get(baseURL + "ticketinfo" + "?ticket_id=" + myId.toString(),
        headers: {
          "Content-Type": "application/json",
          "lang": "ar",
          "Authorization": token
        }).then((response) {
      Map map = json.decode(response.body);
      print(map);
      if (response.statusCode == 200) {
        categoriesList.add(map['result']['ticket_info']);
        setState(() {
          loading = false;

          calculateTime(categoriesList[0]['date'], categoriesList[0]['time']);
          startTimer();
        });
      } else if (response.statusCode == 401) {
        print("false");
      } else if (response.statusCode == 444) {
        print("false");
      } else if (response.statusCode == 500) {
        print("false");
      } else {
        print("false");
      }
    });
  }

  void cancelReserve() async {
    setState(() {
      loading = true;
    });

    String token = await getToken();

    http.delete(baseURL + "book/" + myId.toString(), headers: {
      "Content-Type": "application/json",
      "lang": "ar",
      "Authorization": token
    }).then((response) {
      Map map = json.decode(response.body);
      print(map);
      if (response.statusCode == 200) {
        setState(() {
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => MyApp()));
        });
      } else if (response.statusCode == 401) {
        print("false");
      } else if (response.statusCode == 444) {
        print("false");
      } else if (response.statusCode == 500) {
        print("false");
      } else {
        print("false");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xFF202334),
        title: Center(
          child:
              Text(AppLocalizations.of(context).translate("reserve_details")),
        ),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Theme.Colors.white,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0, left: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(
                  Icons.search,
                  size: 26.0,
                  color: Theme.Colors.mainColor,
                ),
              )),
        ],
      ),
      body: SingleChildScrollView(
        child: !loading
            ? Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      // The containers in the background
                      Container(
                        color: Theme.Colors.white,
                        child: ClipRRect(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(25.0),
                                bottomRight: Radius.circular(25.0)),
                            child: Container(
                              height: 100.0,
                              width: double.infinity,
                              color: Theme.Colors.mainColor,
                            )),
                      ),
                      Container(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 40,
                            ),
                            Container(
                              color: Theme.Colors.white,
                              margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8.0)),
                                  border:
                                      Border.all(color: Theme.Colors.grey300),
                                ),
                                margin: EdgeInsets.only(
                                  bottom: 10,
                                ),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.all(10),
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                            color: Theme.Colors.white,
                                            padding: EdgeInsets.only(
                                              top: 3,
                                              bottom: 3,
                                              right: 3,
                                              left: 3,
                                            ),
                                            child: Image.network(
                                              categoriesList[0]['location']
                                                  ['image'],
                                              width: 70,
                                              height: 70,
                                            ),
                                          ),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Icon(
                                                    Icons.date_range,
                                                    size: 20,
                                                    color: Theme.Colors.red,
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Text(
                                                    categoriesList[0]['date'],
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                      color: Theme.Colors.black,
                                                      fontSize: 14,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Icon(
                                                    Icons.access_time_rounded,
                                                    size: 20,
                                                    color: Theme.Colors.red,
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Text(
                                                    categoriesList[0]['time'],
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                      color:
                                                          Theme.Colors.black54,
                                                      fontSize: 14,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    Text(
                                      AppLocalizations.of(context)
                                              .translate("branch") +
                                          " " +
                                          categoriesList[0]['branch'],
                                      style: TextStyle(
                                        color: Theme.Colors.black,
                                        fontSize: 24,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    CircularStepProgressIndicator(
                                      totalSteps: 10,
                                      currentStep: _currentStep,
                                      stepSize: 22,
                                      selectedColor:
                                          Theme.Colors.darkAccentColor,
                                      unselectedColor: Theme.Colors.grey300,
                                      padding: pi / 80,
                                      width: 170,
                                      height: 170,
                                      startingAngle: 180,
                                      arcSize: 6.28,
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        SizedBox(
                                          width: 20,
                                        ),
                                        Text(
                                          AppLocalizations.of(context)
                                              .translate("now_serve"),
                                          style: TextStyle(
                                            color: Theme.Colors.grey900,
                                            fontSize: 20,
                                          ),
                                        ),
                                        new GestureDetector(
                                          onTap: () {},
                                          child: new Text(
                                            categoriesList[0]
                                                    ['serving_customers']
                                                .toString(),
                                            style: TextStyle(
                                              color:
                                                  Theme.Colors.darkAccentColor,
                                              fontSize: 20,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 20,
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(
                                          Icons.person,
                                          size: 30,
                                          color: Theme.Colors.darkAccentColor,
                                        ),
                                        Text(
                                          "...",
                                          style: TextStyle(
                                            color: Theme.Colors.darkAccentColor,
                                            fontSize: 22,
                                          ),
                                        ),
                                        Icon(
                                          Icons.person,
                                          size: 33,
                                          color: Theme.Colors.grey,
                                        ),
                                        Icon(
                                          Icons.person,
                                          size: 33,
                                          color: Theme.Colors.grey,
                                        ),
                                        Icon(
                                          Icons.person,
                                          size: 33,
                                          color: Theme.Colors.grey,
                                        ),
                                        Icon(
                                          Icons.person,
                                          size: 33,
                                          color: Theme.Colors.grey,
                                        ),
                                        Icon(
                                          Icons.person,
                                          size: 33,
                                          color: Theme.Colors.grey,
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Text(
                                      AppLocalizations.of(context)
                                          .translate("ticket_no"),
                                      style: TextStyle(
                                        color: Theme.Colors.grey,
                                        fontSize: 30,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      categoriesList[0]['number'],
                                      style: TextStyle(
                                        color: Theme.Colors.grey,
                                        fontSize: 40,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      AppLocalizations.of(context)
                                          .translate("platform"),
                                      style: TextStyle(
                                          color: Theme.Colors.grey,
                                          fontSize: 30),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 16, vertical: 10),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Expanded(
                                            child: ClipRRect(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8.0)),
                                                child: InkWell(
                                                  onTap: () {
                                                    Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              UpdateTimeScreen(
                                                                  categoriesList[
                                                                      0]['id'],
                                                                  categoriesList[
                                                                              0]
                                                                          [
                                                                          'location']
                                                                      [
                                                                      'branch_id'])),
                                                    );
                                                  },
                                                  child: Container(
                                                    height: 50.0,
                                                    color: Theme
                                                        .Colors.darkAccentColor,
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceEvenly,
                                                      children: <Widget>[
                                                        Text(
                                                          AppLocalizations.of(
                                                                  context)
                                                              .translate(
                                                                  "postpone"),
                                                          style: TextStyle(
                                                            color: Theme
                                                                .Colors.white,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                )),
                                          ),
                                          SizedBox(
                                            width: 15,
                                          ),
                                          Expanded(
                                            child: ClipRRect(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8.0)),
                                                child: Container(
                                                  height: 50.0,
                                                  color: Theme
                                                      .Colors.darkAccentColor,
                                                  child: InkWell(
                                                    onTap: () {
                                                      MapUtils.openMap(
                                                          categoriesList[0]
                                                                  ['location']
                                                              ['lat'],
                                                          categoriesList[0]
                                                                  ['location']
                                                              ['long']);
                                                    },
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceEvenly,
                                                      children: <Widget>[
                                                        Text(
                                                          AppLocalizations.of(
                                                                  context)
                                                              .translate(
                                                                  "direction"),
                                                          style: TextStyle(
                                                            color: Theme
                                                                .Colors.white,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                )),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 0,
                                    ),
                                    Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 16, vertical: 2),
                                        child: Container(
                                          height: 40,
                                          width: double.infinity,
                                          child: RaisedButton(
                                            onPressed: () {
                                              cancelReserve();
                                            },
                                            color: Theme.Colors.grey,
                                            child: Text(
                                              AppLocalizations.of(context)
                                                  .translate("cancel"),
                                              style: TextStyle(
                                                color: Theme.Colors.white,
                                              ),
                                            ),
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8.0)),
                                            padding: const EdgeInsets.all(0.0),
                                          ),
                                        )),
                                    SizedBox(
                                      height: 25,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "NO - ",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Theme.Colors.grey900,
                            ),
                          ),
                          Text(
                            "Q",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Theme.Colors.darkAccentColor,
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      )
                    ],
                  )
                ],
              )
            : Container(
                height: 500,
                child: Center(
                  child: Container(
                    height: 100,
                    child: Lottie.asset('assets/images/loading.json'),
                  ),
                ),
              ),
      ),
    );
  }
}
