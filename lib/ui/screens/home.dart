import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:kf_drawer/kf_drawer.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:queue/utils/global.dart';
import 'package:queue/ui/screens/category.dart';
import 'package:queue/style/theme.dart' as Theme;

class HomeScreen extends KFDrawerContent {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool loading = true;
  int _current = 0;
  List categoriesList = new List();
  List filteredList = new List();

  @override
  void initState() {
    super.initState();
    homeAPI();
  }

  void homeAPI() async {
    setState(() {
      loading = true;
    });
    http.get(baseURL + "categories", headers: {
      "Content-Type": "application/json",
      "lang": "ar"
    }).then((response) {
      Map map = json.decode(response.body);
      print(map);
      if (response.statusCode == 200) {
        categoriesList = map['result']['categories'];
        setState(() {
          filteredList.addAll(categoriesList);
          imagesList = map['result']['ads'];
          loading = false;
        });
      } else if (response.statusCode == 401) {
        print("false");
      } else if (response.statusCode == 444) {
        print("false");
      } else if (response.statusCode == 500) {
        print("false");
      } else {
        print("false");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                // The containers in the background
                Container(
                  color: Theme.Colors.white,
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25.0),
                      bottomRight: Radius.circular(25.0),
                    ),
                    child: Container(
                      height: 100.0,
                      width: double.infinity,
                      color: Theme.Colors.mainColor,
                    ),
                  ),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                        child: !loading
                            ? GridView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  childAspectRatio:
                                      MediaQuery.of(context).size.width * .0025,
                                ),
                                shrinkWrap: true,
                                itemCount: loading ? 10 : filteredList.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return loading
                                      ? Card(
                                          margin: EdgeInsets.symmetric(
                                            vertical: 10,
                                            horizontal: 5,
                                          ),
                                          elevation: 2,
                                          color: Theme.Colors.white,
                                          child: Center(
                                              child: Column(
                                            children: <Widget>[
                                              Container(
                                                width: double.infinity,
                                                color: Theme.Colors.white,
                                                padding: EdgeInsets.only(
                                                  top: 5,
                                                  bottom: 5,
                                                ),
                                                child: Image.network(
                                                  filteredList[index]['image'],
                                                  width: 50,
                                                  height: 50,
                                                ),
                                              ),
                                              Text(
                                                filteredList[index]['name'],
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  color: Theme.Colors.black87,
                                                ),
                                                maxLines: 1,
                                              ),
                                            ],
                                          )),
                                        )
                                      : InkWell(
                                          onTap: () async {
                                            SharedPreferences prefs =
                                                await SharedPreferences
                                                    .getInstance();
                                            String token =
                                                prefs.getString("token");
                                            if (token != null) {
                                              selectedMainCategoryID =
                                                  filteredList[index]['id']
                                                      .toString();
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        CategoryScreen(
                                                            filteredList[index]
                                                                ['id']),
                                                  ));
                                            } else {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        CategoryScreen(
                                                            filteredList[index]
                                                                ['id'])),
                                              );
                                            }
                                          },
                                          child: Card(
                                            margin: EdgeInsets.symmetric(
                                              vertical: 10,
                                              horizontal: 5,
                                            ),
                                            elevation: 2,
                                            color: Theme.Colors.white,
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(15.0),
                                            ),
                                            child: Center(
                                              child: Column(
                                                children: <Widget>[
                                                  Container(
                                                    margin:
                                                        EdgeInsets.symmetric(
                                                            vertical: 20,
                                                            horizontal: 0),
                                                    width: double.infinity,
                                                    color: Theme.Colors.white,
                                                    padding: EdgeInsets.only(
                                                      top: 5,
                                                      bottom: 5,
                                                    ),
                                                    child: Image.network(
                                                      filteredList[index]
                                                          ['image'],
                                                      width: 50,
                                                      height: 40,
                                                    ),
                                                  ),
                                                  Text(
                                                    filteredList[index]['name'],
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color:
                                                          Theme.Colors.black87,
                                                    ),
                                                    maxLines: 1,
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        );
                                },
                              )
                            : Container(
                                height: 500,
                                child: Center(
                                  child: Container(
                                    height: 100,
                                    child: Lottie.asset(
                                      'assets/images/loading.json',
                                    ),
                                  ),
                                ),
                              ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "NO - ",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Theme.Colors.grey900,
                        ),
                      ),
                      Text(
                        "Q",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Theme.Colors.darkAccentColor,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
