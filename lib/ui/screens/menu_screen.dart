import 'dart:convert';
import 'package:kf_drawer/kf_drawer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:queue/style/theme.dart' as Theme;
import '../../utils/global.dart';
import 'package:queue/ui/screens/home.dart';
import 'language.dart';

class MenuScreen extends StatefulWidget {
  MenuScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> with TickerProviderStateMixin {
  KFDrawerController _drawerController;

  @override
  void initState() {
    super.initState();
    profileAPI();
    print("Aya in side menu");

    if (loginState) {
      _drawerController = KFDrawerController(
        initialPage: HomeScreen(),
        items: [
          KFDrawerItem.initWithPage(
            text: Text(
              'Home',
              style: TextStyle(
                color: Theme.Colors.white,
                fontSize: 12,
              ),
            ),
            icon: Icon(
              Icons.home,
              color: Theme.Colors.white,
            ),
            page: HomeScreen(),
          ),
          KFDrawerItem.initWithPage(
            text: Text(
              'Tickets',
              style: TextStyle(
                color: Theme.Colors.white,
                fontSize: 12,
              ),
            ),
            icon: Icon(
              Icons.home,
              color: Theme.Colors.white,
            ),
            page: HomeScreen(),
          ),
          KFDrawerItem.initWithPage(
            text: Text(
              'Profile',
              style: TextStyle(
                color: Theme.Colors.white,
                fontSize: 12,
              ),
            ),
            icon: Icon(
              Icons.home,
              color: Theme.Colors.white,
            ),
            page: HomeScreen(),
          ),
          KFDrawerItem.initWithPage(
            text: Text(
              'language',
              style: TextStyle(
                color: Theme.Colors.white,
                fontSize: 12,
              ),
            ),
            icon: Icon(
              Icons.assignment,
              color: Theme.Colors.white,
            ),
            page: HomeScreen(),
          ),
          KFDrawerItem.initWithPage(
            text: Text(
              'sign out',
              style: TextStyle(
                color: Theme.Colors.white,
                fontSize: 12,
              ),
            ),
            icon: Icon(
              Icons.home,
              color: Theme.Colors.white,
            ),
            onPressed: () async {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              prefs.clear();
              loginState = false;
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, "Splash");
            },
          ),
        ],
      );
    } else {
      _drawerController = KFDrawerController(
        initialPage: HomeScreen(),
        items: [
          KFDrawerItem.initWithPage(
            text: Text(
              'Home',
              style: TextStyle(
                color: Theme.Colors.white,
                fontSize: 12,
              ),
            ),
            icon: Icon(
              Icons.home,
              color: Theme.Colors.white,
            ),
            page: HomeScreen(),
          ),
          KFDrawerItem.initWithPage(
            text: Text(
              'Tickets',
              style: TextStyle(
                color: Theme.Colors.white,
                fontSize: 12,
              ),
            ),
            icon: Icon(
              Icons.home,
              color: Theme.Colors.white,
            ),
            page: HomeScreen(),
          ),
          KFDrawerItem.initWithPage(
            text: Text(
              'Profile',
              style: TextStyle(
                color: Theme.Colors.white,
                fontSize: 12,
              ),
            ),
            icon: Icon(
              Icons.home,
              color: Theme.Colors.white,
            ),
            page: HomeScreen(),
          ),
          KFDrawerItem.initWithPage(
            text: Text(
              'language',
              style: TextStyle(
                color: Theme.Colors.white,
                fontSize: 12,
              ),
            ),
            icon: Icon(
              Icons.assignment,
              color: Theme.Colors.white,
            ),
            page: Language(),
          ),
          KFDrawerItem.initWithPage(
            text: Text(
              'sign out',
              style: TextStyle(
                color: Theme.Colors.white,
                fontSize: 12,
              ),
            ),
            icon: Icon(
              Icons.home,
              color: Theme.Colors.white,
            ),
            onPressed: () async {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              prefs.clear();
              loginState = false;
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, "Splash");
            },
          ),
        ],
      );
    }
  }

  void profileAPI() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");

    if (token != null) {
      http.get(baseURL + "profile", headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": token
      }).then((response) {
        Map map = json.decode(response.body);
        print("--------->" + map.toString());
        if (response.statusCode == 200) {
          setState(() {
            profileData = map['result']['info'];
          });
        } else if (response.statusCode == 401) {
          print("false");
        } else if (response.statusCode == 444) {
          print("false");
        } else if (response.statusCode == 500) {
          print("false");
        } else {
          print("false");
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: double.infinity,
            color: Theme.Colors.mainColor,
          ),
          KFDrawer(
            menuPadding: EdgeInsets.all(0.0),
            scrollable: true,
            controller: _drawerController,
            header: Column(
              children: <Widget>[
                Container(
                    child: Stack(
                  children: [
                    Image.asset(
                      "assets/images/background.png",
                      height: 220,
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 30, left: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Image.asset(
                            "assets/images/vectorrr.png",
                            width: 90,
                            height: 90,
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                profileData[0]["name"],
                                style: TextStyle(
                                    color: Theme.Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17),
                              ),
                              Text(
                                "Q@app.com",
                                style: TextStyle(
                                  color: Theme.Colors.white,
                                  fontSize: 15,
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                )),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
            footer: loginState
                ? KFDrawerItem.initWithPage(
                    text: Text(
                      '',
                      style: TextStyle(
                        color: Theme.Colors.white,
                        fontSize: 12,
                      ),
                    ),
                  )
                : KFDrawerItem.initWithPage(
                    text: Text(
                      '',
                      style: TextStyle(
                        color: Theme.Colors.white,
                        fontSize: 12,
                      ),
                    ),
                  ),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Colors.transparent, Colors.transparent],
                tileMode: TileMode.repeated,
              ),
            ),
          )
        ],
      ),
    );
  }
}
