import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'package:http/http.dart' as http;
import 'package:queue/utils/app_localizations.dart';
import 'package:queue/style/theme.dart' as Theme;

class BranchesMapScreen extends KFDrawerContent {
  @override
  _BranchesMapScreenState createState() => _BranchesMapScreenState();
}

class _BranchesMapScreenState extends State<BranchesMapScreen> {
  bool loading = true;
  final LatLng _center = const LatLng(29.521563, 30.677433);
  GoogleMapController mapController;
  BitmapDescriptor icon;
  Set<Marker> markers = Set();

  @override
  void initState() {
    getIcons();
    super.initState();
  }

  getIcons() async {
    final http.Response response =
        await http.get("https://img.icons8.com/plasticine/2x/image.png");
    this.icon = BitmapDescriptor.fromBytes(response.bodyBytes);
    setState(() {
      markers.addAll([
        Marker(
            infoWindow: InfoWindow(
                title: "Aya Baghdadi", snippet: "Lat 29.52358 - Lng 30.677675"),
            draggable: true,
            markerId: MarkerId('value'),
            icon: icon,
            position: LatLng(29.521563, 30.677433)),
        Marker(
            infoWindow: InfoWindow(
                title: "Aya Baghdadi",
                snippet: "Lat 29.5552358 - Lng 30.477675"),
            draggable: true,
            markerId: MarkerId('value2'),
            icon: icon,
            position: LatLng(29.522574, 30.677554)),
//          Marker(
//              infoWindow: InfoWindow(
//                  title: "Mi Ubicacion",
//                  snippet:"Lat 29.52358 - Lng 30.677675"),
//              draggable: true,
//              markerId: MarkerId('value2'),
//              icon: icon,
//              position: LatLng(29.523585, 30.677675)),
//          Marker(
//              infoWindow: InfoWindow(
//                  title: "Mi Ubicacion",
//                  snippet:
//                  "Lat 29.52358 - Lng 30.677675"),
//              draggable: true,
//              markerId: MarkerId('value2'),
//              icon: BitmapDescriptor.defaultMarker,
//              position: LatLng(29.524596, 30.677796)),
      ]);
    });
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.Colors.mainColor,
        title: Center(
          child: Text(AppLocalizations.of(context).translate("branch")),
        ),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Theme.Colors.white,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(
                  Icons.search,
                  size: 26.0,
                ),
              )),
        ],
      ),
      body: GoogleMap(
        onMapCreated: _onMapCreated,
        initialCameraPosition: CameraPosition(
          target: _center,
          zoom: 15.0,
        ),
        markers: markers,
      ),
    );
  }
}
