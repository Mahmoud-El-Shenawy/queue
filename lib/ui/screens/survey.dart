import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';
import 'package:queue/main.dart';
import 'package:queue/style/theme.dart' as Theme;
import 'package:queue/ui/screens/home_container.dart';
import 'package:queue/ui/screens/sign_in.dart';
import 'package:queue/ui/screens/ticket_info.dart';
import 'package:queue/ui/widgets/flushbar_view.dart';
import 'package:queue/ui/widgets/loading_view.dart';
import 'package:queue/utils/app_localizations.dart';
import 'package:queue/utils/global.dart';
import 'package:http/http.dart' as http;
import 'package:queue/model/question_model.dart';
import 'package:queue/utils/sharedPrefrence.dart';

class SurveyScreen extends StatefulWidget {
  @override
  _SurveyScreenState createState() => _SurveyScreenState();
}

class _SurveyScreenState extends State<SurveyScreen> {
  bool loading = true;
  int myReserveId;
  TextEditingController _controller = TextEditingController();
  List categoriesList = List();
  bool visibilityObs = false;
  File image;
  int _groupValue;
  String _value;
  DateTime picked;
  DateTime selectedDate = DateTime.now();
  String date;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      doRequestAPI();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  validationInput() {
    loadingScreen(context);
    if (image == null) {
      flushBarView(context,
          AppLocalizations.of(context).translate("upload_image"), false);
    } else if (date == null) {
      flushBarView(
          context, AppLocalizations.of(context).translate("pick_date"), false);
    } else if (_controller.text.toString().isEmpty) {
      flushBarView(
          context, AppLocalizations.of(context).translate("fill_text"), false);
    } else if (_groupValue == null) {
      flushBarView(context,
          AppLocalizations.of(context).translate("choose_answer"), false);
    } else {
      setState(() {
        postPhoto();
        postData();
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.Colors.mainColor,
        elevation: 0,
        title: Center(
          child: Text(
            AppLocalizations.of(context).translate("survey"),
          ),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Theme.Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: SizedBox(
              width: 40,
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Wrap(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                // The containers in the background
                Container(
                  color: Theme.Colors.white,
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25.0),
                      bottomRight: Radius.circular(25.0),
                    ),
                    child: Container(
                      height: 70.0,
                      width: double.infinity,
                      color: Theme.Colors.mainColor,
                    ),
                  ),
                ),
                // SizedBox(height: 40),
                Container(
                  margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                  child: !loading
                      ? ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: loading ? 0 : categoriesList.length,
                          // ignore: missing_return
                          itemBuilder: (BuildContext context, int index) {
                            if (categoriesList[index]['type'] == "image") {
                              return Center(
                                  child: Column(
                                children: <Widget>[
                                  Container(
                                    child: Column(
                                      children: [
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 16,
                                          ),
                                          child: Container(
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  categoriesList[index]
                                                      ['question'],
                                                  style: TextStyle(
                                                    color: Theme.Colors.grey800,
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 8),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 16,
                                          ),
                                          child: TextFormField(
                                            key: _formKey,
                                            autofocus: false,
                                            focusNode:
                                                AlwaysDisabledFocusNode(),
                                            keyboardType: TextInputType.url,
                                            decoration: InputDecoration(
                                              hintText: image == null
                                                  ? AppLocalizations.of(context)
                                                      .translate("upload_image")
                                                  : AppLocalizations.of(context)
                                                      .translate(
                                                          'upload_successfully'),
                                              contentPadding:
                                                  EdgeInsets.all(10.0),
                                              border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10.0),
                                              ),
                                              suffixIcon: GestureDetector(
                                                onTap: () {
                                                  getImage();
                                                },
                                                child: Icon(
                                                  Icons.camera_alt_rounded,
                                                ),
                                              ),
                                            ),
                                            onSaved: (String value) {},
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ));
                            } else if (categoriesList[index]['type'] ==
                                "date") {
                              return Center(
                                  child: Column(
                                children: <Widget>[
                                  Container(
                                    child: Column(
                                      children: [
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 16,
                                          ),
                                          child: Container(
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  categoriesList[index]
                                                      ['question'],
                                                  style: TextStyle(
                                                    color: Theme.Colors.grey800,
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 8),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 16,
                                          ),
                                          child: TextFormField(
                                              focusNode:
                                                  AlwaysDisabledFocusNode(),
                                              autofocus: false,
                                              decoration: InputDecoration(
                                                filled: true,
                                                hintText: date == null
                                                    ? AppLocalizations.of(
                                                            context)
                                                        .translate("pick_date")
                                                    : date,
                                                contentPadding:
                                                    EdgeInsets.all(10.0),
                                                border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                    10.0,
                                                  ),
                                                ),
                                                suffixIcon: GestureDetector(
                                                  onTap: () {
                                                    _selectDate(context);
                                                  },
                                                  child: Icon(
                                                    Icons.date_range_rounded,
                                                  ),
                                                ),
                                              ),
                                              onSaved: (String value) {}),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ));
                            } else if (categoriesList[index]['type'] ==
                                "text") {
                              return Center(
                                  child: Column(
                                children: <Widget>[
                                  Container(
                                    child: Column(
                                      children: [
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 16,
                                          ),
                                          child: Container(
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  categoriesList[index]
                                                      ['question'],
                                                  style: TextStyle(
                                                    color: Theme.Colors.grey800,
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 8),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 16,
                                          ),
                                          child: TextFormField(
                                            controller: _controller,
                                            autofocus: false,
                                            keyboardType: TextInputType.url,
                                            decoration: InputDecoration(
                                              hintText:
                                                  AppLocalizations.of(context)
                                                      .translate("fill_text"),
                                              contentPadding:
                                                  EdgeInsets.all(10.0),
                                              border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10.0),
                                              ),
                                            ),
                                            onSaved: (String value) {},
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ));
                            } else {
                              return Center(
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      child: Column(
                                        children: [
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                              horizontal: 16,
                                            ),
                                            child: Container(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    categoriesList[index]
                                                        ['question'],
                                                    style: TextStyle(
                                                      color:
                                                          Theme.Colors.grey800,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          SizedBox(height: 8),
                                          Wrap(
                                            children: [
                                              Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                  horizontal: 16,
                                                ),
                                                child: Wrap(
                                                  children: [
                                                    TextFormField(
                                                      showCursor: false,
                                                      autofocus: false,
                                                      focusNode:
                                                          AlwaysDisabledFocusNode(),
                                                      decoration:
                                                          InputDecoration(
                                                        contentPadding:
                                                            EdgeInsets.all(
                                                                10.0),
                                                        border:
                                                            OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      10.0),
                                                        ),
                                                        suffixIcon: Wrap(
                                                          children: [
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(8.0),
                                                              child:
                                                                  DropdownButtonFormField(
                                                                isDense: true,
                                                                icon: Icon(Icons
                                                                    .keyboard_arrow_down),
                                                                decoration:
                                                                    InputDecoration
                                                                        .collapsed(
                                                                  hintText: _groupValue ==
                                                                          null
                                                                      ? AppLocalizations.of(
                                                                              context)
                                                                          .translate(
                                                                              "choose_answer")
                                                                      : categoriesList[index]
                                                                              [
                                                                              'answer']
                                                                          [
                                                                          _groupValue],
                                                                ),
                                                                value:
                                                                    _groupValue,
                                                                onChanged:
                                                                    (newValue) {
                                                                  setState(() {
                                                                    _groupValue =
                                                                        newValue;
                                                                    print(
                                                                        _groupValue);
                                                                  });
                                                                },
                                                                items:
                                                                    // List<DropdownMenuItem>(
                                                                    //
                                                                    // ),
                                                                    dropDownList(
                                                                        categoriesList[index]
                                                                            [
                                                                            'answer']),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      onSaved: (String value) {
                                                        setState(() {
                                                          _value = value;
                                                          print(_value);
                                                        });
                                                      },
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }
                          },
                        )
                      : Container(
                          height: 500,
                          child: Center(
                            child: Container(
                              height: 100,
                              child: Lottie.asset('assets/images/loading.json'),
                            ),
                          ),
                        ),
                ),
                SizedBox(height: 35),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 2),
                  child: Container(
                    height: 40,
                    width: double.infinity,
                    child: RaisedButton(
                      onPressed: () async {
                        Map<String, String> map = {};
                        map['result'] = _controller.text +
                            date.toString() +
                            _groupValue.toString();
                        // TODO: Validation Input Before Push Route
                        setState(() {
                          validationInput();
                        });
                      },
                      color: Theme.Colors.accentColor,
                      child: Text(
                        AppLocalizations.of(context).translate("confirm"),
                        style: TextStyle(color: Theme.Colors.white),
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      padding: const EdgeInsets.all(0.0),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "NO - ",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Theme.Colors.grey900,
                            ),
                          ),
                          Text(
                            "Q",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Theme.Colors.darkAccentColor,
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  List<DropdownMenuItem> dropDownList(List<dynamic> list) {
    List<DropdownMenuItem> listItem = [];
    for (var i = 0; i < list.length; i++) {
      var newItem = DropdownMenuItem(
        child: Text(list[i]),
        value: i,
      );
      listItem.add(newItem);
    }
    return listItem;
  }

  _selectDate(BuildContext context) async {
    picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });

    date = selectedDate.year.toString() +
        "-" +
        selectedDate.month.toString() +
        "-" +
        selectedDate.day.toString();
  }

  getImage() async {
    final _imageFile =
        await ImagePicker().getImage(source: ImageSource.gallery);
    setState(() {
      image = File(_imageFile.path);
    });
  }

  doRequestAPI() async {
    // loadingScreen(context);
    var body = jsonEncode({
      "service_id": myReserveId,
    });
    print(body);
    String token = await getToken();
    http.get(baseURL + "question?service_id=7" /* + myReserveId.toString()*/,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Authorization": token
        }).then((response) {
      Map map = json.decode(response.body);
      print(map);
      if (response.statusCode == 200) {
        // Navigator.pop(context);
        flushBarView(context, "Successfully", false);
        setState(() {
          loading = false;
          categoriesList = map['result']['question'];
          print("Ayaaa");
          print(categoriesList);
        });
      } else if (response.statusCode == 401) {
        Navigator.pop(context);
        flushBarView(context, "رقم الجوال او كلمة المرور خاطئة", false);
      } else if (response.statusCode == 422) {
        Navigator.pop(context);
        flushBarView(context, "رقم الجوال موجود بالفعل", false);
      } else if (response.statusCode == 444) {
        Navigator.pop(context);
        flushBarView(context, "444", false);
      } else if (response.statusCode == 500) {
        Navigator.pop(context);
        flushBarView(context, "500", false);
      } else if (response.statusCode == 400) {
        // EXPIRED_TOKEN
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => SignInScreen()));
        print("false");
      } else {
        Navigator.pop(context);
        flushBarView(context, "500", false);
      }
    });
  }

  Future postData() async {
    print("##################SUVERYDATA#####################");
    print(
      'text: ${_controller.text}' +
          '\n' +
          'date: $date' +
          '\n' +
          'select: $_groupValue',
    );
    loadingScreen(context);
    QuestionReq model = new QuestionReq();
    model.question = new List<Question>();
    model.id = '407';
    for (var i = 0; i < categoriesList.length; i++) {
      Question fd = new Question();
      if (categoriesList[i]["type"] == "text") {
        fd.questionId = categoriesList[i]["id"].toString();
        fd.answer = _controller.text.toString();
        model.question.add(fd);
      } else if (categoriesList[i]["type"] == "date") {
        fd.questionId = categoriesList[i]["id"].toString();
        fd.answer = date.toString();
        model.question.add(fd);
      } else if (categoriesList[i]["type"] == "radio") {
        fd.questionId = categoriesList[i]["id"].toString();
        fd.answer = _groupValue.toString();
        model.question.add(fd);
      }
    }
    print(model.toJson());
    var body = jsonEncode(model.toJson());
    String token = await getToken();
    await http.post(baseURL + "answer", body: body, headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": token
    }).then((response) {
      print("##################SUVERYDATA#####################");
      print(response.statusCode);
      Map map = json.decode(response.body);
      print('map: $map');
      if (response.statusCode == 200) {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    TicketInfo(map['result']['ticket_info']['id'])));
        flushBarView(context, "Successfully", false);
        setState(() {
          loading = false;
        });
      } else if (response.statusCode == 444) {
        Navigator.pop(context);
        flushBarView(context, "444", false);
      } else if (response.statusCode == 500) {
        Navigator.pop(context);
        flushBarView(context, "500", false);
      } else if (response.statusCode == 400) {
        // EXPIRED_TOKEN
        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (context) => MyApp()));
        print("false");
      } else {
        Navigator.pop(context);
        flushBarView(context, "500", false);
      }
    });
  }

  Future postPhoto() async {
    print('##################SURVEYPHOTO#####################');
    print('photo: ${image.path}');
    // loadingScreen(context);
    String token = await getToken();
    var headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": token
    };
    var request = http.MultipartRequest('POST', Uri.parse(baseURL + 'answer'));
    QuestionReq model = new QuestionReq();
    model.question = new List<Question>();
    model.id = '407';
    for (var i = 0; i < categoriesList.length; i++) {
      Question fd = new Question();
      if (categoriesList[i]["type"] == "image") {
        request.fields.addAll({
          'id': model.id,
          'question[0][question_id]': fd.questionId =
              categoriesList[i]["id"].toString()
        });
        request.files.add(await http.MultipartFile.fromPath(
            'question[0][answer]', fd.answer = image.path));
      }
    }
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    print('##################SURVEYPHOTO#####################');
    print('Status Code: ${response.statusCode}');
    print('Response: ${response.stream}');
  }
}

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}
