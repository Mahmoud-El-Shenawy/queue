import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:queue/utils/app_localizations.dart';
import 'package:queue/utils/sharedPrefrence.dart';
import 'package:queue/style/theme.dart' as Theme;
import 'package:queue/ui/widgets/app_name.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:queue/ui/widgets/flushbar_view.dart';
import 'package:queue/ui/widgets/loading_view.dart';
import '../../utils/global.dart';
import 'home_container.dart';
import 'otp.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() {
    return _SignInScreenState();
  }
}

class _SignInScreenState extends State<SignInScreen> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _loading = false;
  TextEditingController _phoneController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  FocusNode phoneFocus = FocusNode();
  FocusNode mailFocus = FocusNode();

  @override
  void initState() {
    super.initState();
    removeData();
  }

  userLoginValidation() {
    if (_phoneController.text.isEmpty) {
      flushBarView(context, "من فضلك ادخل رقم الجوال", false);
    } else {
      userLoginAPI();
    }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        color: Theme.Colors.mainColor,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                color: Theme.Colors.mainColor,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 180,
                    ),
                    appName(),
                  ],
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20)),
                child: Container(
                  height: height - 180,
                  color: Theme.Colors.white,
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 20),
                      Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 16,
                            vertical: 8,
                          ),
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  AppLocalizations.of(context)
                                      .translate('sign_in'),
                                  style: TextStyle(
                                    color: Theme.Colors.grey900,
                                    fontSize: 20,
                                  ),
                                ),
                              ],
                            ),
                          )),
                      SizedBox(height: 30),
                      Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  AppLocalizations.of(context)
                                      .translate('phone'),
                                ),
                              ],
                            ),
                          )),
                      Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 16, vertical: 8),
                          child: Container(
                            height: 45,
                            child: TextField(
                              controller: _phoneController,
                              //  Image.asset("assets/images/noun_smartphon.png", width: 14, height: 14, color: Colors.grey[900],),
                              decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.mobile_screen_share,
                                    color: Theme.Colors.grey900,
                                  ),
                                  contentPadding: EdgeInsets.all(10.0),
                                  border: OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(10.0),
                                    ),
                                  ),
                                  focusColor: Theme.Colors.amber,
                                  hoverColor: Theme.Colors.blueGray,
                                  filled: true,
                                  hintStyle:
                                      TextStyle(color: Theme.Colors.grey800),
                                  hintText: AppLocalizations.of(context)
                                      .translate('enter_phone'),
                                  fillColor: Theme.Colors.white70),
                              keyboardType: TextInputType.number,
                              cursorColor: Theme.Colors.red,
                            ),
                          )),
                      SizedBox(height: 20),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 8),
                        child: Container(
                          height: 40,
                          width: double.infinity,
                          child: RaisedButton(
                            onPressed: () {
                              userLoginValidation();
                            },
                            color: Theme.Colors.accentColor,
                            child: Text(
                              AppLocalizations.of(context).translate('sign_in'),
                              style: TextStyle(
                                color: Theme.Colors.white,
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            padding: const EdgeInsets.all(0.0),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 16,
                          vertical: 2,
                        ),
                        child: Container(
                          height: 40,
                          width: double.infinity,
                          child: RaisedButton(
                            onPressed: () {
                              Navigator.of(context)
                                  .pushReplacement(MaterialPageRoute(
                                builder: (context) => ClientHomeNavigation(),
                              ));
                            },
                            color: Theme.Colors.mainColor,
                            child: Text(
                              AppLocalizations.of(context)
                                  .translate('guest_login'),
                              style: TextStyle(
                                color: Theme.Colors.white,
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            padding: const EdgeInsets.all(0.0),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.center,
                      //   children: <Widget>[
                      //     Text(
                      //       AppLocalizations.of(context).translate('no_account'),
                      //       style: TextStyle(
                      //           color: Colors.grey[900], fontSize: 12),
                      //     ),
                      //     new GestureDetector(
                      //       onTap: () {
                      //         Navigator.of(context).pushReplacement(
                      //             MaterialPageRoute(
                      //                 builder: (context) => SingUpScreen()));
                      //       },
                      //       child: new Text(
                      //         AppLocalizations.of(context).translate('press_here'),
                      //         style: TextStyle(
                      //             color: Color(0xFF477B72), fontSize: 12),
                      //       ),
                      //     ),
                      //   ],
                      // ),
                      // SizedBox(height: 20),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  userLoginAPI() {
    loadingScreen(context);
    print(_phoneController.text);
    print(_passwordController.text);
    var body = jsonEncode({
      "phone": _phoneController.text,
    });
    http.post(baseURL + "auth/login", body: body, headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      // "lang": "ar"
    }).then((response) {
      Map map = json.decode(response.body);
      print(map);
      if (response.statusCode == 200) {
        Navigator.pop(context);
        Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) => OTPScreen(_phoneController.text)));
      } else if (response.statusCode == 401) {
        Navigator.pop(context);
        flushBarView(context, "رقم الجوال او كلمة المرور خاطئة", false);
      } else if (response.statusCode == 422) {
        Navigator.pop(context);
        flushBarView(context, "رقم الجوال موجود بالفعل", false);
      } else if (response.statusCode == 444) {
        Navigator.pop(context);
        flushBarView(context, "444", false);
      } else if (response.statusCode == 500) {
        Navigator.pop(context);
        flushBarView(context, "500", false);
      } else {
        Navigator.pop(context);
        flushBarView(context, "500", false);
      }
    });
  }

  void loginSuccess(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (token != null) {
      loginState = true;
      prefs.setString("token", token);
      Navigator.pushReplacementNamed(context, "Home");
    } else {
      Navigator.pushNamed(context, "Verify");
    }
  }

  @override
  void dispose() {
    super.dispose();
  }
}
