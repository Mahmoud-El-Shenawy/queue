import 'dart:convert';
import 'package:queue/style/theme.dart' as Theme;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'package:queue/ui/screens/survey.dart';
import 'package:queue/ui/screens/ticket_info.dart';
import 'package:queue/utils/app_localizations.dart';
import 'package:queue/utils/sharedPrefrence.dart';
import 'package:http/http.dart' as http;
import 'package:queue/ui/widgets/flushbar_view.dart';
import 'package:queue/ui/widgets/loading_view.dart';
import '../../utils/global.dart';
import 'sign_in.dart';

class BookForSomeoneElseScreen extends KFDrawerContent {
  final int myServiceId;
  final int myId;
  final String time;
  final String date;

  BookForSomeoneElseScreen(this.myServiceId, this.myId, this.time, this.date);

  _BookForSomeoneElse createState() =>
      _BookForSomeoneElse(this.myServiceId, this.myId, this.time, this.date);
}

class _BookForSomeoneElse extends State<BookForSomeoneElseScreen> {
  bool loading = true;
  int _current = 0;
  int myServiceId;
  int myId;
  String time;
  String date;

  _BookForSomeoneElse(this.myServiceId, this.myId, this.time, this.date);

  @override
  void initState() {
    super.initState();
  }

  userLoginValidation() {
    if (_nameController.text.isEmpty) {
      flushBarView(context, "من فضلك ادخل الاسم", false);
    } else if (_phoneController.text.isEmpty) {
      flushBarView(context, "من فضلك ادخل رقم الجوال", false);
    } else {
      reseveAPI();
    }
  }

  reseveAPI() async {
    String token = await getToken();
    loadingScreen(context);
    var isService = "";
    if (myServiceId == 0) {
      isService = "false";
    } else {
      isService = "true";
    }
    var body = jsonEncode({
      "branch_id": myId,
      "time": time,
      "date": date,
      "is_service": isService,
      "service_id": myServiceId,
      "is_other": "true",
      "other_name": _nameController.text,
      "other_phone": _phoneController.text,
    });
    print(body);
    http.post(baseURL + "book", body: body, headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "lang": "ar",
      "Authorization": token
    }).then((response) {
      Map map = json.decode(response.body);
      print(map);
      if (response.statusCode == 200) {
        Navigator.pop(context);
        if (map['result']['ticket_info']['question'] == "true") {
          // Services questions
          // TODO: The new screen push from here to new route
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SurveyScreen()));
          flushBarView(context, "Successfully", false);

        } else {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) =>
              // TODO: Back TicketInfo and Delete SurveyScreen (Done)
              TicketInfo(map['result']['ticket_info']['id']),
            ),
          );
        }
      } else if (response.statusCode == 401) {
        Navigator.pop(context);
        flushBarView(context, "رقم الجوال او كلمة المرور خاطئة", false);
      } else if (response.statusCode == 422) {
        Navigator.pop(context);
        flushBarView(context, "رقم الجوال موجود بالفعل", false);
      } else if (response.statusCode == 444) {
        Navigator.pop(context);
        flushBarView(context, "444", false);
      } else if (response.statusCode == 500) {
        Navigator.pop(context);
        flushBarView(context, "500", false);
      } else if (response.statusCode == 400) {
        // EXPIRED_TOKEN
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => SignInScreen()));
        print("false");
      } else {
        Navigator.pop(context);
        flushBarView(context, "500", false);
      }
    });
  }

  TextEditingController _phoneController = TextEditingController();
  TextEditingController _nameController = TextEditingController();

  List categoriesList = List();
  List filteredList = List();
  String dropdownValue = 'One';
  bool visibilityObs = false;

  void _changed(bool visibility) {
    setState(() {
      visibilityObs = visibility;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.Colors.mainColor,
        title: Center(
          child: Text(AppLocalizations.of(context).translate("book_another")),
        ),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Theme.Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: SizedBox(
              width: 40,
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            // The containers in the background
            Container(
              color: Theme.Colors.white,
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(25.0),
                  bottomRight: Radius.circular(25.0),
                ),
                child: Container(
                  height: 70.0,
                  width: double.infinity,
                  color: Theme.Colors.mainColor,
                ),
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      AppLocalizations.of(context).translate("user_name"),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              child: Container(
                height: 45,
                child: TextField(
                  controller: _nameController,
                  decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.person,
                      color: Theme.Colors.grey900,
                    ),
                    contentPadding: EdgeInsets.all(10.0),
                    border: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(10.0),
                      ),
                    ),
                    focusColor: Theme.Colors.amber,
                    hoverColor: Theme.Colors.blueGray,
                    filled: true,
                    hintStyle: TextStyle(color: Theme.Colors.grey800),
                    hintText: AppLocalizations.of(context)
                        .translate("enter_user_name"),
                    fillColor: Theme.Colors.white70,
                  ),
                  cursorColor: Theme.Colors.red,
                ),
              ),
            ),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      AppLocalizations.of(context).translate("phone"),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              child: Container(
                height: 45,
                child: TextField(
                  controller: _phoneController,
                  decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.mobile_screen_share,
                      color: Theme.Colors.grey900,
                    ),
                    contentPadding: EdgeInsets.all(10.0),
                    border: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(10.0),
                      ),
                    ),
                    focusColor: Theme.Colors.amber,
                    hoverColor: Theme.Colors.blueGray,
                    filled: true,
                    hintStyle: TextStyle(color: Theme.Colors.grey800),
                    hintText:
                        AppLocalizations.of(context).translate("enter_phone"),
                    fillColor: Theme.Colors.white70,
                  ),
                  keyboardType: TextInputType.number,
                  cursorColor: Theme.Colors.red,
                ),
              ),
            ),
            SizedBox(height: 60),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 2),
              child: Container(
                height: 40,
                width: double.infinity,
                child: RaisedButton(
                  onPressed: () {
                    userLoginValidation();
                  },
                  color: Theme.Colors.accentColor,
                  child: Text(
                    AppLocalizations.of(context).translate("confirm"),
                    style: TextStyle(color: Theme.Colors.white),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  padding: const EdgeInsets.all(0.0),
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "NO - ",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Theme.Colors.grey900,
                        ),
                      ),
                      Text(
                        "Q",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Theme.Colors.darkAccentColor,
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
