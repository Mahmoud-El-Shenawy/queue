import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'file:///C:/Users/Mahmoud/Desktop/QueueOver/queue/lib/ui/screens/sign_in.dart';
import 'file:///C:/Users/Mahmoud/Desktop/QueueOver/queue/lib/ui/screens/update_for_someone.dart';
import 'package:queue/utils/app_localizations.dart';
import 'package:queue/utils/sharedPrefrence.dart';
import 'package:http/http.dart' as http;
import 'package:queue/style/theme.dart' as Theme;
import 'package:queue/ui/widgets/flushbar_view.dart';
import 'package:queue/ui/widgets/loading_view.dart';
import '../../utils/global.dart';
import '../../main.dart';

class UpdateTimeScreen extends KFDrawerContent {
  int myId;
  int myReserveId;

  UpdateTimeScreen(this.myReserveId, this.myId);

  @override
  _UpdateTimeScreenState createState() =>
      _UpdateTimeScreenState(this.myReserveId, this.myId);
}

class _UpdateTimeScreenState extends State<UpdateTimeScreen>
    with TickerProviderStateMixin {
  bool loading = true;
  int myId;
  int myReserveId;

  _UpdateTimeScreenState(this.myReserveId, this.myId);

  List categoriesList = List();
  List filteredList = List();
  String dropdownValue = 'One';
  bool visibilityObs = false;
  String selectedStringDate = "";
  String selectedStringTime = "";
  String _verticalGroupValue = "";
  AnimationController controller;
  CurvedAnimation curve;
  String myDate;
  String myEarliestTime = "";
  List<String> _status;
  List<String> _times = [];

  @override
  void initState() {
    super.initState();
    // DateTime now = DateTime.now();
    // myDate = now.year.toString() +
    //     "-" +
    //     now.month.toString() +
    //     "-" +
    //     now.day.toString();
    doRequestAPI(1);
    controller = AnimationController(
      duration: const Duration(milliseconds: 2000),
      vsync: this,
    );
    curve = CurvedAnimation(parent: controller, curve: Curves.easeIn);
  }

  reseveAPI() async {
    loadingScreen(context);
    var is_service = "";
    var time = "";
    var date = "";
    // if (myServiceId == 0){
    //   is_service = "false" ;
    // }else{
    //   is_service = "true" ;
    // }
    if (_verticalGroupValue
        .contains(AppLocalizations.of(context).translate("another_time"))) {
      time = selectedStringTime;
      date = selectedStringDate;
    } else {
      time = myEarliestTime;
      date = myDate;
    }
    var body = jsonEncode({
      "id": myReserveId,
      "time": time,
      "date": date,
    });
    print(body);
    String token = await getToken();

    http.put(baseURL + "book/" + myReserveId.toString(), body: body, headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "lang": "ar",
      "Authorization": token
    }).then((response) {
      Map map = json.decode(response.body);
      print(map);
      if (response.statusCode == 200) {
        Navigator.pop(context);
        flushBarView(context, "Successfully", false);
        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (context) => MyApp()));
      } else if (response.statusCode == 401) {
        Navigator.pop(context);
        flushBarView(context, "رقم الجوال او كلمة المرور خاطئة", false);
      } else if (response.statusCode == 422) {
        Navigator.pop(context);
        flushBarView(context, "رقم الجوال موجود بالفعل", false);
      } else if (response.statusCode == 444) {
        Navigator.pop(context);
        flushBarView(context, "444", false);
      } else if (response.statusCode == 500) {
        Navigator.pop(context);
        flushBarView(context, "500", false);
      } else if (response.statusCode == 400) {
        // EXPIRED_TOKEN
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => SignInScreen(myId)));
        print("false");
      } else {
        Navigator.pop(context);
        flushBarView(context, "500", false);
      }
    });
  }

  doRequestAPI(int firstOrNot) async {
    setState(() {
      loading = true;
    });
    print("Aya0");
    print(baseURL +
        "branch" +
        "?branch_id=" +
        myId.toString() +
        "&date=" +
        myDate);
    http.get(
        baseURL +
            "branch" +
            "?branch_id=" +
            myId.toString() +
            "&date=" +
            myDate,
        headers: {
          "Content-Type": "application/json",
          "lang": "ar"
        }).then((response) {
      Map map = json.decode(response.body);
      print("Aya1");
      print(map);
      print(response.statusCode);
      if (response.statusCode == 200) {
        categoriesList.add(map['result']['branch']);
        setState(() {
          loading = false;
          if (firstOrNot == 1) {
            setState(() {
              myEarliestTime = map['result']['branch']['avilable_time'][0];
              _status = [
                AppLocalizations.of(context).translate("earliest") +
                    myEarliestTime,
                AppLocalizations.of(context).translate("another_time")
              ];

              print(_status);
            });
          } else {
            setState(() {
              _times = [];
              for (int i = 0;
                  i < map['result']['branch']['avilable_time'].length;
                  i++) {
                _times.add(map['result']['branch']['avilable_time'][i]);
              }
            });
            print("Aya4");
            print(_times);
            // Select another time
          }
        });
      } else {
        print("false");
      }
    });
  }

  void _changed(bool visibility) {
    setState(() {
      visibilityObs = visibility;
    });
  }

  int selectedDay = DateTime.now().weekday;
  TimeOfDay selectedTime = TimeOfDay.now();

  DateTime selectedDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        selectedStringDate = selectedDate.year.toString() +
            "-" +
            selectedDate.month.toString() +
            "-" +
            selectedDate.day.toString();
        _times = [];
        selectedStringTime = "";
        doRequestAPI(0);
      });
  }

  @override
  Widget build(BuildContext context) {
    _status = [
      AppLocalizations.of(context).translate("earliest") + myEarliestTime,
      AppLocalizations.of(context).translate("another_time")
    ];

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.Colors.mainColor,
        title: Center(
          child: Text(AppLocalizations.of(context).translate("select_time")),
        ),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Theme.Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: SizedBox(
              width: 40,
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          // Aya Baghdadi
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            // The containers in the background
            Container(
              color: Theme.Colors.white,
              child: ClipRRect(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25.0),
                      bottomRight: Radius.circular(25.0)),
                  child: Container(
                    height: 70.0,
                    width: double.infinity,
                    color: Theme.Colors.mainColor,
                  )),
            ),
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: EdgeInsets.only(left: 20.0, right: 20.0),
              child: Text(
                AppLocalizations.of(context).translate("plz_select"),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              children: <Widget>[
                RadioGroup<String>.builder(
                  groupValue: _verticalGroupValue,
                  onChanged: (value) => setState(() {
                    print(_verticalGroupValue);
                    _verticalGroupValue = value;
                    print(value);
                    if (value.contains(AppLocalizations.of(context)
                        .translate("another_time"))) {
                      _changed(true);
                      controller.forward();
                    } else {
                      _changed(false);
                      controller.reverse();
                    }
                  }),
                  items: _status,
                  itemBuilder: (item) => RadioButtonBuilder(
                    item,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            visibilityObs
                ? FadeTransition(
                    opacity: curve,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16,
                        vertical: 10,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child: InkWell(
                            onTap: () {
                              _selectDate(context);
                            },
                            child: Container(
                              color: Theme.Colors.white,
                              child: ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8.0)),
                                  child: Container(
                                    height: 50.0,
                                    color: Theme.Colors.grey300,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Icon(
                                          Icons.date_range,
                                          color: Theme.Colors.grey900,
                                        ),
                                        Text(selectedStringDate),
                                        Icon(
                                          Icons.arrow_drop_down,
                                          color: Theme.Colors.grey900,
                                        ),
                                      ],
                                    ),
                                  )),
                            ),
                          )),
                          SizedBox(
                            width: 15,
                          ),
                          Expanded(
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  if (selectedStringDate == "") {
                                    flushBarView(
                                        context,
                                        AppLocalizations.of(context)
                                            .translate("add_date"),
                                        false);
                                  } else {}
                                });
                              },
                              child: Container(
                                child: ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8.0)),
                                    child: Container(
                                      height: 50.0,
                                      color: Theme.Colors.grey300,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          Icon(
                                            Icons.access_time,
                                            color: Theme.Colors.grey900,
                                          ),
                                          Text(selectedStringTime),
                                          DropdownButton<String>(
                                            items: _times.map((String value) {
                                              return DropdownMenuItem<String>(
                                                value: value,
                                                child: new Text(value),
                                                onTap: () {
                                                  setState(() {
                                                    selectedStringTime = value;
                                                  });
                                                },
                                              );
                                            }).toList(),
                                            onChanged: (_) {},
                                          ),
                                        ],
                                      ),
                                    )),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ))
                : SizedBox(
                    width: 0,
                  ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: 30,
                ),
                Text(
                  AppLocalizations.of(context).translate("book_another"),
                  style: TextStyle(color: Theme.Colors.grey900, fontSize: 12),
                ),
                GestureDetector(
                  onTap: () {
                    if (_verticalGroupValue.contains(
                            AppLocalizations.of(context)
                                .translate("another_time")) &&
                        selectedStringDate == "" &&
                        selectedStringTime == "") {
                      flushBarView(
                          context,
                          AppLocalizations.of(context)
                              .translate("add_date_time"),
                          false);
                    } else {
                      var time = "";
                      var date = "";
                      if (_verticalGroupValue.contains(
                          AppLocalizations.of(context)
                              .translate("another_time"))) {
                        time = selectedStringTime;
                        date = selectedStringDate;
                      } else {
                        time = myEarliestTime;
                        date = myDate;
                      }
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => UpdateForSomeOne(
                            myReserveId,
                            myId,
                            time,
                            date,
                          ),
                        ),
                      );
                    }
                  },
                  child: Text(
                    AppLocalizations.of(context).translate("press_here"),
                    style: TextStyle(
                      color: Theme.Colors.accentColor,
                      fontSize: 12,
                    ),
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
              ],
            ),
            SizedBox(height: 30),
            Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 2),
                child: Container(
                  height: 40,
                  width: double.infinity,
                  child: RaisedButton(
                    onPressed: () {
                      if (_verticalGroupValue.contains(
                              AppLocalizations.of(context)
                                  .translate("another_time")) &&
                          selectedStringDate == "" &&
                          selectedStringTime == "") {
                        flushBarView(
                            context,
                            AppLocalizations.of(context)
                                .translate("add_date_time"),
                            false);
                      } else {}
                      reseveAPI();
                    },
                    color: Theme.Colors.accentColor,
                    child: Text(
                      AppLocalizations.of(context).translate("confirm"),
                      style: TextStyle(color: Theme.Colors.white),
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    padding: const EdgeInsets.all(0.0),
                  ),
                )),
            SizedBox(
              height: 30,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "NO - ",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Theme.Colors.grey900),
                      ),
                      Text(
                        "Q",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Theme.Colors.darkAccentColor,
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
