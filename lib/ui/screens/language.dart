import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'package:queue/utils/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';

class Language extends KFDrawerContent {
  @override
  _languageState createState() => _languageState();
}

class _languageState extends State<Language>{
  bool loading = true;

  String _verticalGroupValue = "English";
  List<String> _status = ["English", "اللغة العربية"];

  @override
  void initState() {
    super.initState();

    getLanguage();
  }

  List categoriesList = new List();
  List filteredList = new List();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xFF202334),
        title: Center(
          child: Text(AppLocalizations.of(context).translate("language")),
        ),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () {
              if (Navigator.canPop(context)) {
                Navigator.pop(context);
              } else {
                SystemNavigator.pop();
              }
            }),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: SizedBox(width: 40,),),
        ],
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            // The containers in the background
            Container(
              color: Colors.white,
              child: ClipRRect(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25.0),
                      bottomRight: Radius.circular(25.0)),
                  child: Container(
                    height: 70.0,
                    width: double.infinity,
                    color: Color(0xFF202334),
                  )),
            ),
            SizedBox(height: 30,),
            Padding(
              padding: EdgeInsets.only(left: 20.0, right: 20.0),
              child: Text(AppLocalizations.of(context).translate("choose_lang"),),),
            SizedBox(height: 10,),
            Column(
              children: <Widget>[
                RadioGroup<String>.builder(
                  groupValue: _verticalGroupValue,
                  onChanged: (value) => setState(() {
                    _verticalGroupValue = value;
                    print(value);
                    _saveLanguage(value);
                  }),
                  items: _status,
                  itemBuilder: (item) => RadioButtonBuilder(
                    item,
                  ),
                ),
              ],
            ),
            SizedBox(height: 30,),
            Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 10,),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("NO - ", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.grey[900]),),
                    Text("Q", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Color(0xFF477B72)),)
                  ],
                ),
                SizedBox(height: 10,)
              ],
            )
          ],
        ),
      )
    );
  }

  Future<void> _saveLanguage(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(value == "English")
      value = "en";
    else
      value = "ar";
    prefs.setString("lang", value);
    print(value);

    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>MyApp()));
  }

  Future<void> getLanguage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String language = prefs.getString("lang");
    if(language != null){
      setState(() {
        if(language == "ar")
          _verticalGroupValue = "العربية";
        else
          _verticalGroupValue = "English";
      });
    }
  }
}