import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:queue/ui/screens/tickets.dart';
import 'package:queue/ui/screens/profile.dart';
import 'package:queue/utils/app_localizations.dart';
import 'package:queue/utils/sharedPrefrence.dart';
import 'package:http/http.dart' as http;
import 'package:queue/style/theme.dart' as Theme;
import '../../utils/global.dart';
import 'home_container.dart';
import 'sign_in.dart';
import 'language.dart';

class ClientMenuScreen extends StatefulWidget {
  @override
  _ClientMenuScreenState createState() => _ClientMenuScreenState();
}

class _ClientMenuScreenState extends State<ClientMenuScreen> {
  bool loading = false;
  String name = 'Q app';
  String email = 'Q@app.com';
  String image;
  List categoriesList = List();
  bool isAuthorized = true;

  @override
  void initState() {
    super.initState();
    print("-----------------------------------------");
    print("Menu");
    print("-----------------------------------------");
    Future.delayed(Duration.zero, () {
      doRequestAPI();
      print("-----------------------------------------");
      print("Menu 2");
      print("-----------------------------------------");
    });
    print("-----------------------------------------");
    print("Menu 3");
    print("-----------------------------------------");
  }

  void doRequestAPI() async {
    String token = await getToken();
    setState(() {
      loading = true;
    });
    print(token);
    http.get(baseURL + "profile", headers: {
      "Content-Type": "application/json",
      "lang": "ar",
      "Authorization": token
    }).then((response) {
      Map map = json.decode(response.body);
      print(map);
      print("First Aya");
      print(response.statusCode);
      if (response.statusCode == 200) {
        print("Status code is equal 200");
        setState(() {
          name = map['result']['info']['name'];
          email = map['result']['info']['email'];
          image = map['result']['info']['image'];
          print("aaaaaaaaa");
          loading = false;
        });
      } else if (response.statusCode == 401) {
        print("false");
      } else if (response.statusCode == 444) {
        print("false");
      } else if (response.statusCode == 500) {
        print("false");
      } else if (response.statusCode == 400) {
        // EXPIRED_TOKEN
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => SignInScreen()));
        print("false");
      } else {
        print("false");
      }
    });
  }

  final widthBox = SizedBox(
    width: 16.0,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(color: Theme.Colors.mainColor),
        child: SafeArea(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Spacer(),
              Stack(
                children: [
                  Image.asset(
                    "assets/images/background.png",
                    height: 220,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 30, left: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                          ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: CachedNetworkImage(
                              // TODO: Profile Place Holder Image
                              fit: BoxFit.cover,
                              width: 60,
                              height: 60,
                              placeholder: (context, url) =>
                                  CircularProgressIndicator(),
                              imageUrl: image,
                            ),
                          ),
                        // Image.asset(
                        //   "assets/images/vectorrr.png",
                        //   width: 90,
                        //   height: 90,
                        // ),
                        SizedBox(
                          width: 5,
                        ),
                        Wrap(
                          direction: Axis.vertical,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width / 2.7,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    // TODO: Push the user name
                                    name,
                                    maxLines: 2,
                                    style: TextStyle(
                                      color: Theme.Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 17,
                                    ),
                                  ),
                                  Text(
                                    // TODO: Push the user email
                                    email,
                                    maxLines: 2,
                                    style: TextStyle(
                                      color: Theme.Colors.white,
                                      fontSize: 15,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
              Spacer(),
              Spacer(),
              Spacer(),
              Spacer(),
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / 2,
                padding:
                    const EdgeInsets.only(bottom: 8.0, left: 24.0, right: 24.0),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: const EdgeInsets.only(bottom: 10, top: 16),
                        child: InkWell(
                          onTap: () =>
                              Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => ClientHomeNavigation(),
                          )),
                          child: Row(
                            children: [
                              Icon(
                                FontAwesomeIcons.home,
                                color: Theme.Colors.white,
                                size: 20,
                              ),
                              SizedBox(
                                width: 16,
                              ),
                              Text(
                                AppLocalizations.of(context).translate('home'),
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Theme.Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(bottom: 10, top: 16),
                        child: InkWell(
                          onTap: () => Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => TicketsScreen(),
                            ),
                          ),
                          child: Row(
                            children: [
                              Image.asset(
                                "assets/images/receipt.png",
                                color: Theme.Colors.white,
                                width: 20,
                              ),
                              SizedBox(
                                width: 16,
                              ),
                              Text(
                                AppLocalizations.of(context)
                                    .translate('tickets'),
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Theme.Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(bottom: 10, top: 16),
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => Profile(),
                            ));
                          },
                          child: Row(
                            children: [
                              Image.asset(
                                "assets/images/path.png",
                                color: Theme.Colors.white,
                                width: 20,
                              ),
                              SizedBox(
                                width: 16,
                              ),
                              Text(
                                AppLocalizations.of(context)
                                    .translate('profile'),
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Theme.Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(bottom: 10, top: 16),
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => Language()));
                          },
                          child: Row(
                            children: [
                              Image.asset(
                                "assets/images/lang.png",
                                color: Theme.Colors.white,
                                width: 20,
                              ),
                              SizedBox(
                                width: 16,
                              ),
                              Text(
                                AppLocalizations.of(context)
                                    .translate('language'),
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Theme.Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(bottom: 10, top: 16),
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => SignInScreen()));
                          },
                          child: Row(
                            children: [
                              Image.asset(
                                "assets/images/logout.png",
                                color: Theme.Colors.white,
                                width: 20,
                              ),
                              SizedBox(
                                width: 16,
                              ),
                              Text(
                                AppLocalizations.of(context)
                                    .translate('sign_out'),
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Theme.Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Spacer(),
              Spacer(),
              Spacer(),
              Spacer(),
              Spacer(),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}
