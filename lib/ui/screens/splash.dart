import 'dart:async';
import 'package:flutter/material.dart';
import 'package:queue/ui/widgets/slider_splash.dart';
import 'package:queue/utils/sharedPrefrence.dart';
import 'home_container.dart';
import 'package:queue/style/theme.dart' as Theme;

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() {
    return _SplashScreenState();
  }
}

class _SplashScreenState extends State<SplashScreen> {
  Timer timer;

  @override
  void initState() {
    super.initState();
  }

  void enterSuccess() async {
    var token = await getToken();
    print("Aya");
    print(token);
    if (token != null && token != "") {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => ClientHomeNavigation()));
    } else {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => SliderSplash()));
    }
  }

  @override
  Widget build(BuildContext context) {
    timer = new Timer(const Duration(milliseconds: 2000), () async {
      // Navigator.push(
      //   context,
      //   MaterialPageRoute(builder: (context) => branchesMap()),
      // );
      //  Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>SliderScreen()));
      enterSuccess();
    });
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        width: width,
        height: height,
        color: Theme.Colors.mainColor,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "NO - ",
                style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Theme.Colors.white),
              ),
              Text(
                "Q",
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: Theme.Colors.darkAccentColor,
                ),
              )
            ],
          )
//          Image.asset(
//              "assets/images/logoDesign.png"
//          )
          ,
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }
}
