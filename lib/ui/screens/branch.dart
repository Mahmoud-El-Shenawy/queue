import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'package:http/http.dart' as http;
import 'package:queue/ui/screens/sign_in.dart';
import 'package:queue/ui/screens/survey.dart';
import 'package:queue/utils/app_localizations.dart';
import 'package:queue/utils/sharedPrefrence.dart';
import '../../utils/global.dart';
import 'package:queue/style/theme.dart' as Theme;
import 'choose_time.dart';

class BranchScreen extends KFDrawerContent {
  final int myId;

  BranchScreen(this.myId);

  @override
  _BranchState createState() => _BranchState(this.myId);
}

class _BranchState extends State<BranchScreen> {
  int myId;
  _BranchState(this.myId);
  bool loading = true;
  List categoriesList =  List();
  List filteredList =  List();
  List serviceList =  List();
  String _verticalGroupValue = "";
  List<String> _status = List();
  int _verticalGroupValueId = 0;
  int _current = 0;

  @override
  void initState() {
    //  getIcons();
    super.initState();
    doRequestAPI();
  }

  final LatLng _center = const LatLng(29.521563, 30.677433);
  GoogleMapController mapController;
  BitmapDescriptor icon;
  Set<Marker> markers = Set();
  bool isMap = false;

  getIcons() async {
    final http.Response response =
        await http.get("https://img.icons8.com/plasticine/2x/marker.png");
    this.icon = BitmapDescriptor.fromBytes(response.bodyBytes);
    setState(() {
      var count = 0;
      for (var post in filteredList) {
        count = count + 1;
        print(count);
        print(post['location']['lat'] + " g " + post['location']['long']);
        var myMarker = Marker(
          infoWindow: InfoWindow(
              onTap: () {
                doRequestGetServicesAPI(post['id'].toString());
              },
              title: post['name'],
              snippet: post['open_time'] + " : " + post['close_time']),
          draggable: true,
          markerId: MarkerId(post['id'].toString()),
          icon: icon,
          // position: LatLng(30.016893, 31.377033),
          position: LatLng(double.parse(post['location']['lat']),
              double.parse(post['location']['lat'])),
        );
        markers.add(myMarker);
      }
    });
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  void doRequestAPI() async {
    setState(() {
      loading = true;
    });
    http.get(baseURL + "branches" + "?provider_id=" + myId.toString(),
        headers: {
          "Content-Type": "application/json",
          "lang": "ar"
        }).then((response) {
      Map map = json.decode(response.body);
      print(map);
      if (response.statusCode == 200) {
        categoriesList = map['result']['branches'];
        setState(() {
          filteredList.addAll(categoriesList);
          imagesList = map['result']['ads'];
          loading = false;
          getIcons();
        });
      } else if (response.statusCode == 401) {
        print("false");
      } else if (response.statusCode == 444) {
        print("false");
      } else if (response.statusCode == 500) {
        print("false");
      } else if (response.statusCode == 400) {
        // EXPIRED_TOKEN
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => SignInScreen()));
        print("false");
      } else {
        print("false");
      }
    });
  }

  void doRequestGetServicesAPI(String branchId) async {
    setState(() {
      loading = true;
    });

    String token = await getToken();
    http.get(baseURL + "service" + "?branch_id=" + branchId, headers: {
      "Content-Type": "application/json",
      "lang": "ar",
      "Authorization": token
    }).then((response) {
      Map map = json.decode(response.body);
      print(map);
      print(response.statusCode);
      if (response.statusCode == 200) {
        setState(() {
          _status = List();
          serviceList = map['result']['services'];
          print(serviceList.length);
          if (serviceList.length != 0) {
            // Has data appear service list
            for (int i = 0; i < serviceList.length; i++) {
              _status.add(serviceList[i]['name']);
            }
            print(_status);
            print(_status.length);
            _verticalGroupValue = serviceList[0]['name'];
            _verticalGroupValueId = serviceList[0]['id'];
            bottomSheet(context, branchId);
          } else {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      ChooseTimeScreen(_verticalGroupValueId, int.parse(branchId))),
            );
          }
          loading = false;
        });
      } else if (response.statusCode == 401) {
        print("false");
      } else if (response.statusCode == 444) {
        print("false");
      } else if (response.statusCode == 500) {
        print("false");
      } else if (response.statusCode == 400) {
        // EXPIRED_TOKEN
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => SignInScreen()));
        print("false");
      } else {
        print("false");
      }
    });
  }

  void callAgain(String branchId) async {
    Navigator.pop(context);
    bottomSheet(context, branchId);
  }

  void bottomSheet(BuildContext context, String branchId) {
    showModalBottomSheet<void>(
      isScrollControlled: true,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        ),
      ),
      builder: (BuildContext context) {
        return Padding(
          padding: EdgeInsets.only(right: 20.0, left: 20.0),
          child: Container(
            height: 400,
            decoration: new BoxDecoration(
              color: Theme.Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(12.0),
                  topLeft: Radius.circular(12.0)),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  AppLocalizations.of(context).translate('select_service'),
                  style: TextStyle(color: Theme.Colors.black87, fontSize: 16),
                ),
                Container(
                  height: 250,
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: 1,
                    itemBuilder: (context, index) {
                      return InkWell(
                        child: Container(
                          height: (serviceList.length * 50.0),
                          decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            border: Border.all(color: Theme.Colors.grey300),
                          ),
                          margin: EdgeInsets.only(
                            bottom: 10,
                          ),
                          child: Wrap(
                            children: [
                              Container(
                                padding: EdgeInsets.all(10),
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      width: MediaQuery.of(context).size.width -
                                          62,
                                      child: RadioGroup<String>.builder(
                                        groupValue: _verticalGroupValue,
                                        items: _status,
                                        itemBuilder: (item) =>
                                            RadioButtonBuilder(
                                          item,
                                        ),
                                        onChanged: (value) => setState(() {
                                          _verticalGroupValue = value;
                                          for (int i = 0;
                                              i < serviceList.length;
                                              i++) {
                                            if (serviceList[i]['name'] ==
                                                value) {
                                              _verticalGroupValueId =
                                                  serviceList[i]['id'];
                                            }
                                          }
                                          print(value);
                                          callAgain(branchId);
                                        }),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        onTap: () {
                          // Create Service request here & check if had data appear service & if not go to time
                          doRequestGetServicesAPI(
                              filteredList[index]['id'].toString());
                        },
                      );
                    },
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              // TODO: back screen (Done)
                              ChooseTimeScreen(
                            _verticalGroupValueId,
                            int.parse(branchId),
                          ),
                        ),
                      );
                    },
                    child: Container(
                      decoration: new BoxDecoration(
                        color: Theme.Colors.accentColor,
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      ),
                      height: 40,
                      width: 340,
                      child: Center(
                          child: Text(
                              AppLocalizations.of(context).translate('next'),
                              style: TextStyle(
                                color: Theme.Colors.white,
                              ))),
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Theme.Colors.mainColor,
          title: Center(
            child: Text(AppLocalizations.of(context).translate("branches")),
          ),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Theme.Colors.white,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              }),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0, left: 20.0),
                child: GestureDetector(
                    onTap: () {
                      setState(() {
                        isMap = !isMap;
                      });
                    },
                    child: isMap
                        ? Icon(
                            Icons.list,
                            color: Theme.Colors.green,
                            size: 30.0,
                          )
                        : Icon(
                            Icons.map,
                            color: Theme.Colors.grey,
                            size: 30.0,
                          )))
          ],
        ),
        body: isMap
            ? GoogleMap(
                onMapCreated: _onMapCreated,
                initialCameraPosition: CameraPosition(
                  target: _center,
                  zoom: 7.0,
                ),
                markers: markers,
              )
            : Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      // The containers in the background
                      Container(
                        color: Theme.Colors.white,
                        child: ClipRRect(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(25.0),
                                bottomRight: Radius.circular(25.0)),
                            child: Container(
                              height: 100.0,
                              width: double.infinity,
                              color: Theme.Colors.mainColor,
                            )),
                      ),
                      Container(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 40,
                            ),
                            !loading
                                ? Container(
                                    color: Theme.Colors.white,
                                    margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                                    child: ListView.builder(
                                      shrinkWrap: true,
                                      physics: NeverScrollableScrollPhysics(),
                                      itemCount: filteredList.length,
                                      itemBuilder: (context, index) {
                                        return InkWell(
                                          child: Container(
                                            height: 90,
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8.0)),
                                                border: Border.all(
                                                    color:
                                                        Theme.Colors.grey300)),
                                            margin: EdgeInsets.only(
                                              bottom: 10,
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.all(10),
                                              child: Row(
                                                children: <Widget>[
                                                  Container(
                                                    color: Theme.Colors.white,
                                                    padding: EdgeInsets.only(
                                                        top: 3, bottom: 3),
                                                    child: Image.network(
                                                      filteredList[index]
                                                          ['image'],
                                                      width: 70,
                                                      height: 70,
                                                    ),
                                                  ),
                                                  Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        filteredList[index]
                                                            ['name'],
                                                        style: TextStyle(
                                                            color: Theme
                                                                .Colors.black54,
                                                            fontSize: 14),
                                                      ),
                                                      SizedBox(
                                                        height: 5,
                                                      ),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons
                                                                .bookmark_border,
                                                            size: 20,
                                                            color: Theme
                                                                .Colors.red,
                                                          ),
                                                          SizedBox(
                                                            width: 5,
                                                          ),
                                                          Text(
                                                            filteredList[index]
                                                                        ['id']
                                                                    .toString() +
                                                                " km",
                                                            textAlign:
                                                                TextAlign.start,
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black54,
                                                                fontSize: 14),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          onTap: () {
                                            // Create Service request here & check if had data appear service & if not go to time
                                            doRequestGetServicesAPI(
                                                filteredList[index]['id']
                                                    .toString());
                                          },
                                        );
                                      },
                                    ),
                                  )
                                : Container(
                                    height: 500,
                                    child: Center(
                                      child: Container(
                                        height: 100,
                                        child: Image.asset(
                                          'assets/images/logoDesign.json',
                                        ),
                                        // child: Lottie.asset('assets/images/loading.json'),
                                      ),
                                    ),
                                  ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "NO - ",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Theme.Colors.grey900),
                            ),
                            Text(
                              "Q",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Theme.Colors.darkAccentColor),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        )
                      ],
                    ),
                  ),
                ],
              ));
  }
}
