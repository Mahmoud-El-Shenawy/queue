import 'dart:convert';
import 'package:queue/utils/app_localizations.dart';
import 'package:queue/utils/sharedPrefrence.dart';
import 'package:queue/ui/widgets/app_name.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:queue/ui/widgets/flushbar_view.dart';
import 'package:queue/ui/widgets/loading_view.dart';
import '../../utils/global.dart';
import 'package:http/http.dart' as http;
import 'home_container.dart';
import 'sign_in.dart';

class SingUpScreen extends StatefulWidget {
  final String myPhone;
  final String myToken;

  SingUpScreen(this.myPhone, this.myToken);

  @override
  _SingUpScreenState createState() {
    return _SingUpScreenState(this.myPhone, this.myToken);
  }
}

class _SingUpScreenState extends State<SingUpScreen> {
  String myToken;
  String myPhone;

  _SingUpScreenState(this.myPhone, this.myToken);

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _loading = false;
  TextEditingController _phoneController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _nameController = new TextEditingController();

  FocusNode phoneFocus = FocusNode();
  FocusNode mailFocus = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  userLoginValidation() {
    if (_emailController.text.isEmpty) {
      flushBarView(context, "من فضلك ادخل البريد الالكتروني", false);
    } else if (_nameController.text.isEmpty) {
      flushBarView(context, "من فضلك ادخل الاسم", false);
    } else {
      userLoginAPI();
    }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        color: Color(0xFF202334),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                color: Color(0xFF202334),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 140,
                    ),
                    appName(),
                  ],
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
                child: Container(
                  height: height - 140,
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 8),
                        child: Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                AppLocalizations.of(context)
                                    .translate('create_account'),
                                style: TextStyle(
                                    color: Colors.grey[900], fontSize: 20),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                AppLocalizations.of(context)
                                    .translate('user_name'),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 16,
                          vertical: 8,
                        ),
                        child: Container(
                          height: 40,
                          child: TextField(
                            controller: _nameController,
                            decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.person,
                                  color: Colors.grey[900],
                                ),
                                contentPadding: EdgeInsets.all(10.0),
                                border: OutlineInputBorder(
                                  borderRadius: const BorderRadius.all(
                                    const Radius.circular(10.0),
                                  ),
                                ),
                                focusColor: Colors.amber,
                                hoverColor: Colors.blueGrey,
                                filled: true,
                                hintStyle: TextStyle(color: Colors.grey[800]),
                                hintText: AppLocalizations.of(context)
                                    .translate('enter_user_name'),
                                fillColor: Colors.white70),
                            cursorColor: Colors.red,
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                AppLocalizations.of(context).translate('email'),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 16,
                            vertical: 8,
                          ),
                          child: Container(
                            height: 40,
                            child: TextField(
                              controller: _emailController,
                              decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.email,
                                    color: Colors.grey[900],
                                  ),
                                  contentPadding: EdgeInsets.all(10.0),
                                  border: OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(10.0),
                                    ),
                                  ),
                                  focusColor: Colors.amber,
                                  hoverColor: Colors.blueGrey,
                                  filled: true,
                                  hintStyle: TextStyle(color: Colors.grey[800]),
                                  hintText: AppLocalizations.of(context)
                                      .translate('enter_email'),
                                  fillColor: Colors.white70),
                              cursorColor: Colors.red,
                            ),
                          )),
                      SizedBox(height: 10),
                      Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  AppLocalizations.of(context)
                                      .translate('phone'),
                                ),
                              ],
                            ),
                          )),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 8),
                        child: Container(
                          height: 40,
                          child: TextField(
                            enabled: false,
                            controller: _phoneController,
                            decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.mobile_screen_share,
                                  color: Colors.grey[900],
                                ),
                                contentPadding: EdgeInsets.all(10.0),
                                border: OutlineInputBorder(
                                  borderRadius: const BorderRadius.all(
                                    const Radius.circular(10.0),
                                  ),
                                ),
                                focusColor: Colors.amber,
                                hoverColor: Colors.blueGrey,
                                filled: true,
                                labelText: myPhone,
                                hintStyle: TextStyle(color: Colors.grey[800]),
                                hintText: AppLocalizations.of(context)
                                    .translate('enter_phone'),
                                fillColor: Colors.white70),
                            keyboardType: TextInputType.number,
                            cursorColor: Colors.red,
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 8),
                        child: Container(
                          height: 40,
                          width: double.infinity,
                          child: RaisedButton(
                            onPressed: () {
                              userLoginValidation();
                            },
                            color: Color(0xFF57C3B3),
                            child: Text(
                              AppLocalizations.of(context).translate('create'),
                              style: TextStyle(color: Colors.white),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0)),
                            padding: const EdgeInsets.all(0.0),
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      InkWell(
                        onTap: () {},
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              AppLocalizations.of(context)
                                  .translate('already_have_account'),
                              style: TextStyle(
                                  color: Colors.grey[900], fontSize: 12),
                            ),
                            new GestureDetector(
                              onTap: () {
                                Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                        builder: (context) => SignInScreen()));
                              },
                              child: new Text(
                                AppLocalizations.of(context)
                                    .translate('press_here'),
                                style: TextStyle(
                                    color: Color(0xFF57C3B3), fontSize: 12),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 15),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  userLoginAPI() {
    loadingScreen(context);

    var body = jsonEncode({
      "name": _nameController.text,
      "phone": _phoneController.text,
      "email": _emailController.text,
    });

    print(myToken);
    http.post(baseURL + "auth/register", body: body, headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "lang": "ar",
      "Authorization": "bearer" + myToken
    }).then((response) {
      Map map = json.decode(response.body);
      print(map);
      if (response.statusCode == 200) {
        Navigator.pop(context);
        // if(map['data']['is_verified']==0){
        //   Navigator.pushNamed(context, "Verify");
        // }else{
        loginSuccess(myToken);
        // }
      } else if (response.statusCode == 401) {
        Navigator.pop(context);
        flushBarView(context, "رقم الجوال او كلمة المرور خاطئة", false);
      } else if (response.statusCode == 422) {
        Navigator.pop(context);
        flushBarView(context, "رقم الجوال موجود بالفعل", false);
      } else if (response.statusCode == 444) {
        Navigator.pop(context);
        flushBarView(context, "444", false);
      } else if (response.statusCode == 500) {
        Navigator.pop(context);
        flushBarView(context, "500", false);
      } else {
        Navigator.pop(context);
        flushBarView(context, "500", false);
      }
    });
  }

  void loginSuccess(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (token != null) {
      loginState = true;
      prefs.setString("token", token);
      setToken(token: token);
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => ClientHomeNavigation()));
    } else {
      Navigator.pushNamed(context, "Verify");
    }
  }

  @override
  void dispose() {
    super.dispose();
  }
}
