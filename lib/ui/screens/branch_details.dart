import 'dart:convert';
import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:kf_drawer/kf_drawer.dart';
import 'package:queue/utils/app_localizations.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:queue/style/theme.dart' as Theme;
import '../../utils/global.dart';
import 'branches_map.dart';

class BranchDetailsScreen extends KFDrawerContent {
  @override
  _BranchDetailsState createState() => _BranchDetailsState();
}

class _BranchDetailsState extends State<BranchDetailsScreen>
    with TickerProviderStateMixin {
  bool loading = true;
  int _current = 0;

  @override
  void initState() {
    super.initState();
    homeAPI();
  }

  List categoriesList = new List();
  List filteredList = new List();

  void homeAPI() async {
    setState(() {
      loading = true;
    });
    http.get(baseURL + "branch" + "?branch_id=2" + "&date=2021-12-29",
        headers: {
          "Content-Type": "application/json",
          "lang": "ar"
        }).then((response) {
      Map map = json.decode(response.body);
      print(map);
      if (response.statusCode == 200) {
        categoriesList.add(map['result']['branch']);
        // categoriesList = map['result']['branch'];
        setState(() {
          filteredList.addAll(categoriesList);
          imagesList = map['result']['ads'];
          loading = false;
        });
      } else if (response.statusCode == 401) {
        print("false");
      } else if (response.statusCode == 444) {
        print("false");
      } else if (response.statusCode == 500) {
        print("false");
      } else {
        print("false");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.Colors.mainColor,
        title: Center(
          child: Text(AppLocalizations.of(context).translate("branch")),
        ),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Theme.Colors.white,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0, left: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(
                  Icons.search,
                  size: 26.0,
                  color: Theme.Colors.mainColor,
                ),
              )),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                // The containers in the background
                Container(
                  color: Theme.Colors.white,
                  child: ClipRRect(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(25.0),
                          bottomRight: Radius.circular(25.0)),
                      child: Container(
                        height: 100.0,
                        width: double.infinity,
                        color: Theme.Colors.mainColor,
                      )),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 40,
                      ),
                      Container(
                        color: Theme.Colors.white,
                        margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8.0)),
                              border: Border.all(color: Theme.Colors.grey300)),
                          margin: EdgeInsets.only(
                            bottom: 10,
                          ),
                          child: Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.all(10),
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      color: Theme.Colors.white,
                                      padding: EdgeInsets.only(
                                          top: 3, bottom: 3, right: 3, left: 3),
                                      child: Image.network(
                                        categoriesList[0]['image'],
                                        width: 70,
                                        height: 70,
                                      ),
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          categoriesList[0]['date'],
                                          style: TextStyle(
                                              color: Theme.Colors.black54,
                                              fontSize: 14),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Icon(
                                              Icons.access_time_rounded,
                                              size: 20,
                                              color: Theme.Colors.red,
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              AppLocalizations.of(context)
                                                      .translate("from") +
                                                  categoriesList[0]
                                                      ['open_time'] +
                                                  " " +
                                                  AppLocalizations.of(context)
                                                      .translate("to") +
                                                  categoriesList[0]
                                                      ['close_time'],
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                  color: Theme.Colors.black54,
                                                  fontSize: 14),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Text(
                                categoriesList[0]['name'],
                                style: TextStyle(
                                    color: Theme.Colors.black, fontSize: 24),
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              CircularStepProgressIndicator(
                                totalSteps: 15,
                                currentStep: 13,
                                stepSize: 30,
                                selectedColor: Theme.Colors.darkAccentColor,
                                unselectedColor: Theme.Colors.grey400,
                                padding: pi / 80,
                                width: 150,
                                height: 150,
                                startingAngle: 180,
                                arcSize: 6.28,
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Text(
                                    AppLocalizations.of(context)
                                        .translate("now_serve"),
                                    style: TextStyle(
                                      color: Theme.Colors.grey900,
                                      fontSize: 20,
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {},
                                    child: new Text(
                                      "15",
                                      style: TextStyle(
                                        color: Theme.Colors.darkAccentColor,
                                        fontSize: 20,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    Icons.person,
                                    size: 30,
                                    color: Theme.Colors.darkAccentColor,
                                  ),
                                  Text(
                                    "...",
                                    style: TextStyle(
                                      color: Theme.Colors.darkAccentColor,
                                      fontSize: 22,
                                    ),
                                  ),
                                  Icon(
                                    Icons.person,
                                    size: 33,
                                    color: Theme.Colors.grey,
                                  ),
                                  Icon(
                                    Icons.person,
                                    size: 33,
                                    color: Theme.Colors.grey,
                                  ),
                                  Icon(
                                    Icons.person,
                                    size: 33,
                                    color: Theme.Colors.grey,
                                  ),
                                  Icon(
                                    Icons.person,
                                    size: 33,
                                    color: Theme.Colors.grey,
                                  ),
                                  Icon(
                                    Icons.person,
                                    size: 33,
                                    color: Theme.Colors.grey,
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Text(
                                AppLocalizations.of(context)
                                    .translate("ticket_no"),
                                style: TextStyle(
                                    color: Theme.Colors.grey, fontSize: 30),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                "195",
                                style: TextStyle(
                                    color: Theme.Colors.grey, fontSize: 40),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                AppLocalizations.of(context)
                                    .translate("platform"),
                                style: TextStyle(
                                    color: Theme.Colors.grey, fontSize: 30),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 10),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Expanded(
                                      child: ClipRRect(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(8.0)),
                                          child: Container(
                                            height: 50.0,
                                            color: Theme.Colors.darkAccentColor,
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                Text(
                                                  AppLocalizations.of(context)
                                                      .translate("postpone"),
                                                  style: TextStyle(
                                                      color:
                                                          Theme.Colors.white),
                                                ),
                                              ],
                                            ),
                                          )),
                                    ),
                                    SizedBox(
                                      width: 15,
                                    ),
                                    Expanded(
                                      child: ClipRRect(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(8.0)),
                                          child: Container(
                                            height: 50.0,
                                            color: Theme.Colors.darkAccentColor,
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                Text(
                                                  AppLocalizations.of(context)
                                                      .translate("direction"),
                                                  style: TextStyle(
                                                      color:
                                                          Theme.Colors.white),
                                                ),
                                              ],
                                            ),
                                          )),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 0,
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16, vertical: 2),
                                  child: Container(
                                    height: 40,
                                    width: double.infinity,
                                    child: RaisedButton(
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  BranchesMapScreen()),
                                        );
                                      },
                                      color: Theme.Colors.grey,
                                      child: Text(
                                        AppLocalizations.of(context)
                                            .translate("cancel"),
                                        style: TextStyle(
                                            color: Theme.Colors.white),
                                      ),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8.0)),
                                      padding: const EdgeInsets.all(0.0),
                                    ),
                                  )),
                              SizedBox(
                                height: 25,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 10,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "NO - ",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Theme.Colors.grey900,
                      ),
                    ),
                    Text(
                      "Q",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Theme.Colors.darkAccentColor),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
