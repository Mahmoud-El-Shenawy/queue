import 'dart:convert';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:image_picker/image_picker.dart';
import 'package:queue/main.dart';
import 'package:queue/style/theme.dart' as Theme;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';
import 'package:queue/ui/screens/home_container.dart';
import 'package:queue/ui/screens/survey.dart';
import 'package:queue/ui/widgets/flushbar_view.dart';
import 'package:queue/ui/widgets/loading_view.dart';
import 'package:queue/ui/screens/sign_in.dart';
import 'package:queue/ui/widgets/profile_survey_model.dart';
import 'package:queue/utils/app_localizations.dart';
import 'package:http/http.dart' as http;
import 'package:queue/utils/sharedPrefrence.dart';
import '../../utils/global.dart';
import 'change_password.dart';
import 'home.dart';

class Profile extends StatefulWidget {
  //TODO: Pass Info from Survey + Show IMG
  @override
  _Profile createState() => _Profile();
}

class _Profile extends State<Profile> {
  bool loading = true;
  File _imageProfile;
  File _imageSurvey;
  String token;
  TextEditingController _controller = TextEditingController();
  TextEditingController _controllerName = TextEditingController();
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerPhone = TextEditingController();
  int _groupValue;
  DateTime picked;
  DateTime selectedDate = DateTime.now();
  String date;
  int myReserveId;
  String name;
  String email;
  List categoriesList = List();
  List questionList = List();
  List extraInfoList = List();
  String _valueText = '';
  String _valueDate = '';
  String _valueSelect = '';
  bool _obscureText = true;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      doRequestAPI();
      // fetchQuestions();
    });
  }

  fetchQuestions() async {
    loadingScreen(context);
    var body = jsonEncode({
      "service_id": myReserveId,
    });
    print(body);
    String token = await getToken();
    http.get(baseURL + "question?service_id=7" /* + myReserveId.toString()*/,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Authorization": token
        }).then((response) {
      Map map = json.decode(response.body);
      print(map);
      if (response.statusCode == 200) {
        Navigator.pop(context);
        flushBarView(context, "Successfully", false);
        setState(() {
          loading = false;
          questionList = map['result']['question'];
          print("Ayaaa");
          print(categoriesList);
        });
      } else if (response.statusCode == 401) {
        Navigator.pop(context);
        flushBarView(context, "رقم الجوال او كلمة المرور خاطئة", false);
      } else if (response.statusCode == 422) {
        Navigator.pop(context);
        flushBarView(context, "رقم الجوال موجود بالفعل", false);
      } else if (response.statusCode == 444) {
        Navigator.pop(context);
        flushBarView(context, "444", false);
      } else if (response.statusCode == 500) {
        Navigator.pop(context);
        flushBarView(context, "500", false);
      } else if (response.statusCode == 400) {
        // EXPIRED_TOKEN
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => SignInScreen()));
        print("false");
      } else {
        Navigator.pop(context);
        flushBarView(context, "500", false);
      }
    });
  }

  void doRequestAPI() async {
    setState(() {
      loading = true;
    });
    token = await getToken();
    print(token);
    http.get(baseURL + "profile", headers: {
      "Content-Type": "application/json",
      "lang": "ar",
      "Authorization": token
    }).then((response) {
      Map map = json.decode(response.body);
      print(map);
      print("First Aya");
      print(response.statusCode);
      if (response.statusCode == 200) {
        print("Status code is equal 200");
        categoriesList.add(map['result']['info']);
        extraInfoList = map['result']['extra_info'];
        print("Status code is equal 200 2222");
        print("------------------------------------");
        print("Aya");
        print(categoriesList);
        print(extraInfoList);
        setState(() {
          loading = false;
        });
      } else if (response.statusCode == 401) {
        print("false");
      } else if (response.statusCode == 444) {
        print("false");
      } else if (response.statusCode == 500) {
        print("false");
      } else if (response.statusCode == 400) {
        // EXPIRED_TOKEN
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => SignInScreen()));
        print("false");
      } else {
        print("false");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.Colors.mainColor,
        title: Center(
          child: Text(AppLocalizations.of(context).translate("profile")),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Theme.Colors.white,
          ),
          onPressed: () {
            if (Navigator.canPop(context)) {
              Navigator.of(context).pop();
            } else {
              SystemNavigator.pop();
            }
          },
        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: SizedBox(
              width: 40,
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            // The containers in the background
            Container(
              color: Theme.Colors.white,
              child: ClipRRect(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(25.0),
                    bottomRight: Radius.circular(25.0),
                  ),
                  child: Container(
                    height: 40.0,
                    width: double.infinity,
                    color: Theme.Colors.mainColor,
                  )),
            ),
            Container(
              child: Column(
                children: <Widget>[
                  Container(
                    child: !loading
                        ? Column(
                            children: [
                              SizedBox(
                                height: 20,
                              ),
                              Stack(
                                children: [
                                  Center(
                                    child: Expanded(
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(50),
                                        child: CachedNetworkImage(
                                          // TODO: Profile Place Holder Image
                                          fit: BoxFit.cover,
                                          width: 100,
                                          height: 100,
                                          placeholder: (context, url) =>
                                              CircularProgressIndicator(),
                                          imageUrl: categoriesList[0]['image'],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Center(
                                    child: InkWell(
                                      onTap: () {
                                        setState(() {
                                          getImageProfile();
                                        });
                                      },
                                      child: Container(
                                        width: 110,
                                        height: 80,
                                        child: Align(
                                          alignment: Alignment.bottomRight,
                                          child: Icon(
                                            Icons.camera_enhance_rounded,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 20),
                              Center(
                                child: InkWell(
                                  onTap: () {
                                    Navigator.of(context)
                                        .pushReplacement(MaterialPageRoute(
                                      builder: (context) =>
                                          ChangePasswordScreen(),
                                    ));
                                  },
                                  child: Text(
                                    categoriesList[0]['name'],
                                  ),
                                ),
                              ),
                              SizedBox(height: 10),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 5),
                                child: Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        AppLocalizations.of(context)
                                            .translate("user_name"),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 8),
                                child: TextFormField(
                                    controller: _controllerName,
                                    autofocus: false,
                                    keyboardType: TextInputType.text,
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(
                                        Icons.person,
                                        color: Theme.Colors.grey900,
                                      ),
                                      hintText: AppLocalizations.of(context)
                                          .translate("user_name"),
                                      labelText: name =
                                          categoriesList[0]['name'],
                                      contentPadding: EdgeInsets.all(10.0),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10.0)),
                                      suffixIcon: GestureDetector(
                                        child: Icon(Icons.history_edu),
                                      ),
                                    ),
                                    onSaved: (String value) {}),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 5),
                                child: Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        AppLocalizations.of(context)
                                            .translate("email"),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 8),
                                child: TextFormField(
                                    controller: _controllerEmail,
                                    autofocus: false,
                                    keyboardType: TextInputType.emailAddress,
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(
                                        Icons.email,
                                        color: Theme.Colors.grey900,
                                      ),
                                      hintText: AppLocalizations.of(context)
                                          .translate("email"),
                                      labelText: email =
                                          categoriesList[0]['email'],
                                      contentPadding: EdgeInsets.all(10.0),
                                      border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                      ),
                                      suffixIcon: GestureDetector(
                                        child: Icon(Icons.history_edu),
                                      ),
                                    ),
                                    onSaved: (String value) {}),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 5),
                                child: Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        AppLocalizations.of(context)
                                            .translate("phone"),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 8),
                                child: TextFormField(
                                    controller: _controllerPhone,
                                    autofocus: false,
                                    keyboardType: TextInputType.phone,
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(
                                        Icons.phone_android,
                                        color: Theme.Colors.grey900,
                                      ),
                                      hintText: AppLocalizations.of(context)
                                          .translate("phone"),
                                      labelText: categoriesList[0]['phone'],
                                      contentPadding: EdgeInsets.all(10.0),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10.0)),
                                      suffixIcon: GestureDetector(
                                        child: Icon(Icons.history_edu),
                                      ),
                                    ),
                                    onSaved: (String value) {}),
                              ),
                              Container(
                                child: !loading
                                    ? ListView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount:
                                            loading ? 0 : extraInfoList.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          if (extraInfoList[index]['type'] ==
                                              "image") {
                                            return Center(
                                                child: Column(
                                              children: <Widget>[
                                                Container(
                                                  child: Column(
                                                    children: [
                                                      SizedBox(height: 6),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .symmetric(
                                                          horizontal: 16,
                                                          vertical: 8,
                                                        ),
                                                        child: Container(
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              Text(
                                                                extraInfoList[
                                                                        index]
                                                                    ['name'],
                                                                style:
                                                                    TextStyle(
                                                                  color: Theme
                                                                      .Colors
                                                                      .grey800,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(height: 8),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .symmetric(
                                                          horizontal: 16,
                                                        ),
                                                        child: TextFormField(
                                                          autofocus: false,
                                                          focusNode:
                                                              AlwaysDisabledFocusNode(),
                                                          keyboardType:
                                                              TextInputType.url,
                                                          decoration:
                                                              InputDecoration(
                                                            hintText: _imageSurvey ==
                                                                    null
                                                                ? extraInfoList[
                                                                        index]
                                                                    ['value']
                                                                : _imageSurvey
                                                                    .path,
                                                            contentPadding:
                                                                EdgeInsets.all(
                                                                    10.0),
                                                            border:
                                                                OutlineInputBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                10.0,
                                                              ),
                                                            ),
                                                            suffixIcon:
                                                                GestureDetector(
                                                              onTap: () {
                                                                // TODO: Image Survey
                                                                setState(() {
                                                                  getImageSurvey();
                                                                });
                                                              },
                                                              child: Icon(
                                                                Icons
                                                                    .camera_alt_rounded,
                                                              ),
                                                            ),
                                                          ),
                                                          onSaved:
                                                              (String value) {},
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ));
                                          } else if (extraInfoList[index]
                                                  ['type'] ==
                                              "date") {
                                            return Center(
                                                child: Column(
                                              children: <Widget>[
                                                Container(
                                                  child: Column(
                                                    children: [
                                                      SizedBox(
                                                        height: 10,
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .symmetric(
                                                          horizontal: 16,
                                                        ),
                                                        child: Container(
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              Text(
                                                                extraInfoList[
                                                                        index]
                                                                    ['name'],
                                                                style:
                                                                    TextStyle(
                                                                  color: Theme
                                                                      .Colors
                                                                      .grey800,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(height: 8),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .symmetric(
                                                          horizontal: 16,
                                                        ),
                                                        child: TextFormField(
                                                            focusNode:
                                                                AlwaysDisabledFocusNode(),
                                                            autofocus: false,
                                                            decoration:
                                                                InputDecoration(
                                                              filled: true,
                                                              hintText: date ==
                                                                      null
                                                                  ? extraInfoList[
                                                                          index]
                                                                      ['value']
                                                                  : date,
                                                              contentPadding:
                                                                  EdgeInsets
                                                                      .all(
                                                                          10.0),
                                                              border:
                                                                  OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                  10.0,
                                                                ),
                                                              ),
                                                              suffixIcon:
                                                                  GestureDetector(
                                                                onTap: () {
                                                                  setState(() {
                                                                    _selectDate(
                                                                        context);
                                                                  });
                                                                },
                                                                child: Icon(
                                                                  Icons
                                                                      .date_range_rounded,
                                                                ),
                                                              ),
                                                            ),
                                                            onSaved: (String
                                                                value) {}),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ));
                                          } else if (extraInfoList[index]
                                                  ['type'] ==
                                              "text") {
                                            return Center(
                                                child: Column(
                                              children: <Widget>[
                                                Container(
                                                  child: Column(
                                                    children: [
                                                      SizedBox(
                                                        height: 10,
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .symmetric(
                                                          horizontal: 16,
                                                        ),
                                                        child: Container(
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              Text(
                                                                extraInfoList[
                                                                        index]
                                                                    ['name'],
                                                                style:
                                                                    TextStyle(
                                                                  color: Theme
                                                                      .Colors
                                                                      .grey800,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(height: 8),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .symmetric(
                                                          horizontal: 16,
                                                        ),
                                                        child: TextFormField(
                                                            controller:
                                                                _controller,
                                                            autofocus: false,
                                                            keyboardType:
                                                                TextInputType
                                                                    .text,
                                                            decoration:
                                                                InputDecoration(
                                                              hintText:
                                                                  extraInfoList[
                                                                          index]
                                                                      ['name'],
                                                              labelText:
                                                                  extraInfoList[
                                                                          index]
                                                                      ['value'],
                                                              contentPadding:
                                                                  EdgeInsets
                                                                      .all(
                                                                          10.0),
                                                              border:
                                                                  OutlineInputBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            10.0),
                                                              ),
                                                              suffixIcon:
                                                                  GestureDetector(
                                                                child: Icon(Icons
                                                                    .history_edu),
                                                              ),
                                                            ),
                                                            onSaved: (String
                                                                value) {}),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ));
                                          } else {
                                            return Center(
                                              child: Column(
                                                children: <Widget>[
                                                  Container(
                                                    child: Column(
                                                      children: [
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .symmetric(
                                                            horizontal: 16,
                                                          ),
                                                          child: Container(
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .start,
                                                              children: <
                                                                  Widget>[
                                                                Text(
                                                                  extraInfoList[
                                                                          index]
                                                                      ['name'],
                                                                  style:
                                                                      TextStyle(
                                                                    color: Theme
                                                                        .Colors
                                                                        .grey800,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w500,
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(height: 8),
                                                        Wrap(
                                                          children: [
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .symmetric(
                                                                horizontal: 16,
                                                              ),
                                                              child: Wrap(
                                                                children: [
                                                                  TextFormField(
                                                                    showCursor:
                                                                        false,
                                                                    autofocus:
                                                                        false,
                                                                    focusNode:
                                                                        AlwaysDisabledFocusNode(),
                                                                    decoration:
                                                                        InputDecoration(
                                                                      contentPadding:
                                                                          EdgeInsets.all(
                                                                              10.0),
                                                                      border:
                                                                          OutlineInputBorder(
                                                                        borderRadius:
                                                                            BorderRadius.circular(10.0),
                                                                      ),
                                                                      suffixIcon:
                                                                          Wrap(
                                                                        children: [
                                                                          Padding(
                                                                            padding:
                                                                                const EdgeInsets.all(8.0),
                                                                            child:
                                                                                DropdownButtonFormField(
                                                                              icon: Icon(
                                                                                Icons.keyboard_arrow_down,
                                                                              ),
                                                                              decoration: InputDecoration.collapsed(
                                                                                hintText: extraInfoList[index]['value'],
                                                                              ),
                                                                              value: _groupValue,
                                                                              onChanged: (value) {
                                                                                setState(() {
                                                                                  _valueSelect = value;
                                                                                  print(_valueSelect);
                                                                                });
                                                                              },
                                                                              items: null,
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    onSaved: (String
                                                                        value) {},
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            );
                                          }
                                        })
                                    : Container(
                                        height: 500,
                                        child: Center(
                                          child: Container(
                                            height: 100,
                                            child: Lottie.asset(
                                                'assets/images/loading.json'),
                                          ),
                                        ),
                                      ),
                              ),
                              SizedBox(height: 20),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 2),
                                child: Container(
                                  height: 40,
                                  width: double.infinity,
                                  child: RaisedButton(
                                    onPressed: () async {
                                      loadingScreen(context);
                                      await validationInput();
                                      // Navigator.pop(context);
                                    },
                                    color: Theme.Colors.accentColor,
                                    child: Text(
                                      AppLocalizations.of(context)
                                          .translate("save"),
                                      style: TextStyle(
                                        color: Theme.Colors.white,
                                      ),
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                    padding: const EdgeInsets.all(0.0),
                                  ),
                                ),
                              ),
                            ],
                          )
                        : Container(
                            height: 500,
                            child: Center(
                              child: Container(
                                height: 100,
                                child:
                                    Lottie.asset('assets/images/loading.json'),
                              ),
                            ),
                          ),
                  ),
                ],
              ),
            ),

            SizedBox(
              height: 30,
            ),
            Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 10,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "NO - ",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Theme.Colors.grey900),
                    ),
                    Text(
                      "Q",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Theme.Colors.darkAccentColor),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Future postProfile() async {
    // loadingScreen(context);
    print('##################PROFILE#####################');
    print(
      'name: ${_controllerName.text}' +
          '\n' +
          'email: ${_controllerEmail.text}' +
          '\n' +
          'phone: ${_controllerPhone.text}' +
          '\n' +
          'image: ${_imageProfile.path}',
    );

    String token = await getToken();
    var headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": token
    };
    var request = http.MultipartRequest('POST', Uri.parse(baseURL + 'profile'));
    if (_controllerName.text.toString() != null) {
      request.fields.addAll({
        'name': _controllerName.text.toString(),
      });
    } else if (_controllerEmail.text.toString() != null) {
      request.fields.addAll({
        'email': _controllerEmail.text.toString(),
      });
    } else if (_controllerPhone.text.toString() != null) {
      request.fields.addAll({
        'phone': _controllerPhone.text.toString(),
      });
    }
    request.files
        .add(await http.MultipartFile.fromPath('photo', _imageProfile.path));
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    print('##################PROFILE#####################');
    print('Profile: ${response.statusCode}');
    if (response.statusCode == 200) {
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => ClientHomeNavigation()));
      print(await response.stream.bytesToString());
      flushBarView(context, "Successfully", false);
      setState(() {
        loading = false;
      });
    } else if (response.statusCode == 444) {
      setState(() {
        loading = false;
      });
      // Navigator.pop(context);
      flushBarView(context, "444", false);
    } else if (response.statusCode == 500) {
      setState(() {
        loading = false;
      });
      // Navigator.pop(context);
      flushBarView(context, "500", false);
    } else if (response.statusCode == 400) {
      setState(() {
        loading = false;
      });
      setState(() {
        loading = false;
      });
      print(await response.stream.bytesToString());
      // EXPIRED_TOKEN
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) => MyApp()));
      flushBarView(context, 'Expired Token', false);
      print("false");
    } else {
      setState(() {
        loading = false;
      });
      // Navigator.pop(context);
      print(response.reasonPhrase);
      flushBarView(context, "500", false);
    }
  }

  Future postSurveyPhoto() async {
    // loadingScreen(context);
    print('##################SURVEYPHOTO#####################');
    print('photo: ${_imageSurvey.path}');

    String token = await getToken();
    var headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": token
    };

    var request = http.MultipartRequest('POST', Uri.parse(baseURL + 'profile'));
    for (var i = 0; i < extraInfoList.length; i++) {
      if (extraInfoList[i]['type'] == 'image') {
        request.fields.addAll({
          'extra_info[0][id]': extraInfoList[i]['id'].toString(),
        });
        request.files.add(await http.MultipartFile.fromPath(
          'extra_info[0][answer]',
          _imageSurvey.path,
        ));
      }
    }
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();

    print('##################SURVEYPHOTO#####################');
    print('Photo Survey: ${response.statusCode}');

    if (response.statusCode == 200) {
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => ClientHomeNavigation()));
      print(await response.stream.bytesToString());
      print(response.reasonPhrase);
      flushBarView(context, "Successfully", false);
      setState(() {
        loading = false;
      });
    } else if (response.statusCode == 444) {
      setState(() {
        loading = false;
      });
      // Navigator.pop(context);
      print(response.reasonPhrase);
      flushBarView(context, "444", false);
    } else if (response.statusCode == 500) {
      setState(() {
        loading = false;
      });
      // Navigator.pop(context);
      print(response.reasonPhrase);
      flushBarView(context, "500", false);
    } else if (response.statusCode == 400) {
      print(await response.stream.bytesToString());
      setState(() {
        loading = false;
      });
      // EXPIRED_TOKEN
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) => MyApp()));
      flushBarView(context, 'Expired Token', false);
      print(response.reasonPhrase);
      print("false");
    } else {
      // Navigator.pop(context);
      setState(() {
        loading = false;
      });
      print(response.reasonPhrase);
      flushBarView(context, "500", false);
    }
  }

  Future postSurvey() async {
    // loadingScreen(context);
    print('##################SURVEY#####################');
    print(
      'text: ${_controller.text}' + '\n' + 'date: $date',
    );

    ProfileSurvey model = new ProfileSurvey();
    model.extraInfo = List<ExtraInfo>();
    for (var i = 0; i < extraInfoList.length; i++) {
      ExtraInfo fd = new ExtraInfo();
      if (extraInfoList[i]["type"] == "date") {
        if (date != null) {
          fd.id = extraInfoList[i]["id"].toString();
          fd.answer = date.toString();
          model.extraInfo.add(fd);
        }
      } else if (extraInfoList[i]["type"] == "text") {
        if (_controller.text.isNotEmpty) {
          fd.id = extraInfoList[i]["id"].toString();
          fd.answer = _controller.text.toString();
          model.extraInfo.add(fd);
        }
      }
    }
    print(model.toJson());
    var body = jsonEncode(model.toJson());
    String token = await getToken();
    await http.post(baseURL + "profile", body: body, headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": token
    }).then((response) {
      Map map = json.decode(response.body);
      print('##################SURVEY#####################');
      print('map: $map');
      print('Survey: ${response.statusCode}');
      if (response.statusCode == 200) {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => ClientHomeNavigation()));
        flushBarView(context, "Successfully", false);
        setState(() {
          loading = false;
        });
        print(response.request);
        print(response.reasonPhrase);
      } else if (response.statusCode == 444) {
        setState(() {
          loading = false;
        });
        // Navigator.pop(context);
        flushBarView(context, "444", false);
      } else if (response.statusCode == 500) {
        setState(() {
          loading = false;
        });
        // Navigator.pop(context);
        print(response.reasonPhrase);
        flushBarView(context, "500", false);
      } else if (response.statusCode == 400) {
        setState(() {
          loading = false;
        });
        // EXPIRED_TOKEN
        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (context) => MyApp()));
        print("false");
        flushBarView(context, 'Expired Token', false);
      } else {
        // Navigator.pop(context);
        setState(() {
          loading = false;
        });
        flushBarView(context, "500", false);
      }
    });
  }

  getImageProfile() async {
    final _imageFile =
        await ImagePicker().getImage(source: ImageSource.gallery);
    setState(() {
      _imageProfile = File(_imageFile.path);
      print('Image Profile: ${_imageFile.path}');
    });
  }

  getImageSurvey() async {
    final _imageFile =
        await ImagePicker().getImage(source: ImageSource.gallery);
    setState(() {
      _imageSurvey = File(_imageFile.path);
      print('Image Survey: ${_imageFile.path}');
    });
  }

  validationInput() {
    loadingScreen(context);
    setState(() {
      postProfile();
      postSurvey();
      postSurveyPhoto();
      loading = false;
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => ClientHomeContainer()));
    });
  }

  _selectDate(BuildContext context) async {
    picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });

    date = selectedDate.year.toString() +
        "-" +
        selectedDate.month.toString() +
        "-" +
        selectedDate.day.toString();
  }
}
