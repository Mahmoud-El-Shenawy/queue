import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:kf_drawer/kf_drawer.dart';
import 'package:lottie/lottie.dart';
import 'package:queue/ui/screens/sign_in.dart';
import 'package:queue/ui/screens/ticket_info.dart';
import 'package:queue/utils/app_localizations.dart';
import 'package:queue/utils/sharedPrefrence.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../utils/global.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';

class TicketsScreen extends KFDrawerContent {
  @override
  _TicketsScreenState createState() => _TicketsScreenState();
}

class _TicketsScreenState extends State<TicketsScreen> {
  bool loading = true;
  int _current = 0;
  String myCase = "active";

  @override
  void initState() {
    super.initState();
    _getLocation();
    doRequestAPI();
  }

  _getLocation() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    debugPrint('location: ${position.latitude}');
    final coordinates = new Coordinates(position.latitude, position.longitude);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    print("-------------------------aya-------------");
    print("${first.featureName} : ${first.addressLine}");
  }

  List categoriesList = List();

  void doRequestAPI() async {
    setState(() {
      loading = true;
    });
    String token = await getToken();
    print(token);
    print("tickets");
    http.get(baseURL + "tickets" + "?status=" + myCase, headers: {
      "Content-Type": "application/json",
      "lang": "ar",
      "Authorization": token
    }).then((response) {
      Map map = json.decode(response.body);
      print(map);
      if (response.statusCode == 200) {
        categoriesList = map['result']['tickets'];
        print(categoriesList);
        setState(() {
          loading = false;
        });
      } else if (response.statusCode == 401) {
        print("false");
      } else if (response.statusCode == 444) {
        print("false");
      } else if (response.statusCode == 500) {
        print("false");
      } else if (response.statusCode == 400) {
        // EXPIRED_TOKEN
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => SignInScreen()));
        print("false");
      } else {
        print("false");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xFF202334),
        title: Center(
          child: Text(AppLocalizations.of(context).translate("tickets")),
        ),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0, left: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(
                  Icons.search,
                  size: 26.0,
                  color: Color(0xFF202334),
                ),
              )),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 30,
              color: Color(0xFF202334),
            ),
            Row(
              children: [
                Container(
                  width: 20,
                  height: 64,
                  color: Color(0xFF202334),
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(
                        bottom: 10, top: 10, right: 10, left: 10),
                    width: double.infinity,
                    height: 44,
                    child: RaisedButton(
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      onPressed: () {
                        setState(() {
                          _current = 0;
                          myCase = "active";
                          doRequestAPI();
                        });
                      },
                      color: _current == 0 ? Colors.green : Colors.white,
                      textColor: _current == 0 ? Colors.white : Colors.black,
                      child: Text(
                          AppLocalizations.of(context).translate('current')),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(
                        bottom: 10, top: 10, right: 10, left: 10),
                    width: double.infinity,
                    height: 44,
                    child: RaisedButton(
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      onPressed: () {
                        setState(() {
                          _current = 1;
                          myCase = "exp";
                          doRequestAPI();
                        });
                      },
                      color: _current != 0 ? Colors.green : Colors.white,
                      textColor: _current != 0 ? Colors.white : Colors.black,
                      child: Text(
                          AppLocalizations.of(context).translate('expire')),
                    ),
                  ),
                ),
                Container(
                  width: 20,
                  height: 64,
                  color: Color(0xFF202334),
                ),
              ],
            ),
            Stack(
              children: <Widget>[
                // The containers in the background

                Container(
                  color: Colors.white,
                  child: ClipRRect(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(25.0),
                          bottomRight: Radius.circular(25.0)),
                      child: Container(
                        height: 100.0,
                        width: double.infinity,
                        color: Color(0xFF202334),
                      )),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Container(
                        // margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                        child: !loading
                            ? Container(
                                color: Colors.white,
                                margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemCount: categoriesList.length,
                                  itemBuilder: (context, index) {
                                    return InkWell(
                                      child: Container(
                                        height: 90,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8.0)),
                                            border: Border.all(
                                                color: Colors.grey[300])),
                                        margin: EdgeInsets.only(
                                          bottom: 10,
                                        ),
                                        child: Container(
                                          padding: EdgeInsets.all(10),
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                color: Color(0xFFFFFFFF),
                                                padding: EdgeInsets.only(
                                                    top: 3, bottom: 3),
                                                child: Image.network(
                                                  categoriesList[index]
                                                      ['location']['image'],
                                                  width: 70,
                                                  height: 70,
                                                ),
                                              ),
                                              Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    categoriesList[index]
                                                        ['name'],
                                                    style: TextStyle(
                                                        color: Colors.black54,
                                                        fontSize: 14),
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Row(
                                                    children: <Widget>[
                                                      Icon(
                                                        Icons.location_pin,
                                                        size: 20,
                                                        color: Colors.red,
                                                      ),
                                                      SizedBox(
                                                        width: 5,
                                                      ),
                                                      Text(
                                                        categoriesList[index]
                                                                    ['id']
                                                                .toString() +
                                                            " km",
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                            color:
                                                                Colors.black54,
                                                            fontSize: 14),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      onTap: () async {
                                        SharedPreferences prefs =
                                            await SharedPreferences
                                                .getInstance();
                                        String token = prefs.getString("token");

                                        if (token != null) {
                                          selectedMainCategoryID =
                                              categoriesList[index]['id']
                                                  .toString();
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) => TicketInfo(
                                                  categoriesList[index]['id']),
                                            ),
                                          );
                                        } else {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    TicketInfo(
                                                  categoriesList[index]['id'],
                                                ),
                                              ));
                                        }
                                      },
                                    );
                                  },
                                ),
                              )
                            : Container(
                                height: 500,
                                child: Center(
                                  child: Container(
                                    height: 100,
                                    child: Lottie.asset(
                                        'assets/images/loading.json'),
                                  ),
                                ),
                              ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "NO - ",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[900]),
                      ),
                      Text(
                        "Q",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Color(0xFF477B72)),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
