import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:queue/ui/screens/sign_in.dart';
import 'package:queue/ui/screens/profile.dart';
import 'package:queue/ui/screens/tickets.dart';
import 'package:queue/utils/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:queue/style/theme.dart' as Theme;
import 'client_menu.dart';
import 'package:queue/ui/screens/home.dart';
import 'language.dart';

class ClientHomeContainer extends StatefulWidget {
  static const routName = '/client_home';
  final int specificPage;

  ClientHomeContainer({this.specificPage});

  @override
  _ClientHomeContainerState createState() {
    return _ClientHomeContainerState();
  }
}

class _ClientHomeContainerState extends State<ClientHomeContainer> {
  int _selectedPage;
  static const int _HOME = 0;
  static const int _TICKET = 1;
  static const int _PROFILE = 2;
  static const int _LANGUAGE = 3;
  static const int _SIGN_OUT = 4;
  String title = "home";

  @override
  void initState() {
    super.initState();
    setState(() {
      this._selectedPage = widget.specificPage ?? _HOME;
      _selectedPage == 3 ? title = "profile" : title = "home";
    });
  }

  Widget _body() {
    switch (this._selectedPage) {
      case _HOME:
        return HomeScreen();
      case _TICKET:
        return TicketsScreen();
      case _PROFILE:
        return Profile();
      case _LANGUAGE:
        return Language();
      case _SIGN_OUT:
        return SignInScreen();
    }
    print("index selected out of range ");
    return Container();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return WillPopScope(
      onWillPop: () async {
        print("container will pop scope ");
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
            elevation: 0,
            backgroundColor: Theme.Colors.mainColor,
            title: Center(
              child: Text(AppLocalizations.of(context).translate("home")),
            ),
            leading: Align(
              alignment: AlignmentDirectional.centerStart,
              child: IconButton(
                icon: Image.asset(
                  "assets/images/noun_menu_.png",
                  height: 24,
                  width: 24,
                ),
                onPressed: () => ZoomDrawer.of(context).toggle(),
              ),
            ),
            actions: <Widget>[
              Padding(
                  padding: EdgeInsets.only(right: 20.0, left: 20.0),
                  child: GestureDetector(
                    onTap: () {},
                    child: Icon(
                      Icons.search,
                      size: 26.0,
                      color: Theme.Colors.mainColor,
                    ),
                  )),
            ]),
        body: this._body(),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}

class ClientHomeNavigation extends StatefulWidget {
  final int specificPage;

  ClientHomeNavigation({this.specificPage});

  @override
  _ClientHomeNavigationState createState() {
    return _ClientHomeNavigationState();
  }
}

class _ClientHomeNavigationState extends State<ClientHomeNavigation> {
  final _drawerController = ZoomDrawerController();
  String lang = "";

  @override
  void initState() {
    super.initState();

    getLang();
  }

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      lang = prefs.getString("lang");
      print("language " + lang + " Home Container");
    });
  }

  @override
  Widget build(BuildContext context) {
    return lang == "ar"
        ? ZoomDrawer(
            controller: _drawerController,
            menuScreen: ClientMenuScreen(),
            mainScreen: ClientHomeContainer(
              specificPage: widget.specificPage,
            ),
            borderRadius: 24.0,
            showShadow: true,
            angle: 0.0,
            slideWidth: MediaQuery.of(context).size.width * (-.280),
          )
        : ZoomDrawer(
            controller: _drawerController,
            menuScreen: ClientMenuScreen(),
            mainScreen: ClientHomeContainer(
              specificPage: widget.specificPage,
            ),
            borderRadius: 24.0,
            showShadow: true,
            angle: 0.0,
            slideWidth: MediaQuery.of(context).size.width * (.60),
          );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
