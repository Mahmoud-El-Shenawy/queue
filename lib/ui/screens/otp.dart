import 'dart:convert';
import 'file:///C:/Users/Mahmoud/Desktop/QueueOver/queue/lib/ui/screens/sign_up.dart';
import 'package:queue/utils/app_localizations.dart';
import 'package:queue/utils/sharedPrefrence.dart';
import 'package:queue/ui/widgets//app_name.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:queue/ui/widgets/flushbar_view.dart';
import 'package:queue/ui/widgets/loading_view.dart';
import '../../utils/global.dart';
import 'package:http/http.dart'as http;

import 'home_container.dart';

class OTPScreen extends StatefulWidget {
  final String myPhone;
  OTPScreen(this.myPhone);
  @override
  _OTPScreenState createState() {
    return _OTPScreenState(this.myPhone);
  }
}

class _OTPScreenState extends State<OTPScreen> {
  String myPhone;
  _OTPScreenState(this.myPhone);

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _loading = false;
  TextEditingController _otpController = new TextEditingController();
  FocusNode phoneFocus = FocusNode();
  FocusNode mailFocus = FocusNode();

  @override
  void initState() {
    super.initState();
  }


  userLoginValidation() {
   if (_otpController.text.isEmpty) {
     flushBarView(context, "من فضلك ادخل الرمز المرسل للتآكيد", false);
   } else {
     userLoginAPI();
   }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        color: Color(0xFF202334),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                color: Color(0xFF202334),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 180,
                    ),
                    appName(),
                  ],
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
                child: Container(
                  height: height - 180,
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 20),
                      Padding(
                          padding:
                          const EdgeInsets.symmetric(
                              horizontal: 16, vertical: 8),
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "OTP",
                                  style: TextStyle(
                                      color: Colors.grey[900], fontSize: 20),
                                ),
                              ],
                            ),
                          )),
                      SizedBox(height: 30),
                      Padding(
                          padding:
                          const EdgeInsets.symmetric(horizontal: 16),
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  AppLocalizations.of(context).translate('validation_code'),
                                ),
                              ],
                            ),
                          )),
                      Padding(
                          padding:
                          const EdgeInsets.symmetric(
                              horizontal: 16, vertical: 8),
                          child: Container(
                            height: 45,
                            child: TextField(
                              controller: _otpController,
                              decoration: InputDecoration(
                                  prefixIcon: Icon(Icons.lock_outline,
                                    color: Colors.grey[900],),
                                  contentPadding: EdgeInsets.all(10.0),
                                  border: OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(10.0),
                                    ),
                                  ),
                                  focusColor: Colors.amber,
                                  hoverColor: Colors.blueGrey,
                                  filled: true,
                                  hintStyle: TextStyle(color: Colors.grey[800]),
                                  hintText: AppLocalizations.of(context).translate('enter_otp'),
                                  fillColor: Colors.white70),
                              keyboardType: TextInputType.number,
                              cursorColor: Colors.red,
                            ),
                          )),
                      SizedBox(height: 20),
                      Padding(
                          padding:
                          const EdgeInsets.symmetric(
                              horizontal: 16, vertical: 8),
                          child: Container(
                            height: 40,
                            width: double.infinity,
                            child: RaisedButton(
                              onPressed: () {
                                userLoginValidation();
                              },
                              color: Color(0xFF57C3B3),
                              child: Text(
                                AppLocalizations.of(context).translate('confirm'),
                                style: TextStyle(color: Colors.white),
                              ),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8.0)
                              ),
                              padding: const EdgeInsets.all(0.0),
                            ),
                          )
                      ),
                      SizedBox(height: 40),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  userLoginAPI() {
    loadingScreen(context);
    var body = jsonEncode({
      "phone": myPhone,
      "otp": _otpController.text,
    });

    http.post(baseURL + "auth/check", body: body,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "lang": "ar"
        }).then((response) {
      Map map = json.decode(response.body);
      print(map);
      if (response.statusCode == 200) {
        Navigator.pop(context);

          loginSuccess(map['result']['token'],map['result']['new']);
      } else if (response.statusCode == 401) {
        Navigator.pop(context);
        flushBarView(context, "رقم الجوال او كلمة المرور خاطئة", false);
      } else if (response.statusCode == 422) {
        Navigator.pop(context);
        flushBarView(context, "رقم الجوال موجود بالفعل", false);
      } else if (response.statusCode == 444) {
        Navigator.pop(context);
        flushBarView(context, "444", false);
      } else if (response.statusCode == 500) {
        Navigator.pop(context);
        flushBarView(context, "500", false);
      } else {
        Navigator.pop(context);
        flushBarView(context, "500", false);
      }
    });
  }

  void loginSuccess(String token , String myNew) async {
    print(myNew);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (myNew == "true") {
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => SingUpScreen(myPhone ,token)));
    }else {
      if (token != null && token != "") {
        print("Aya not new");
        loginState = true;
        prefs.setString("token", token);
        setToken(token: token);
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => ClientHomeNavigation()));
        // Navigator.pushReplacementNamed(context, "Home");
      } else {
        // Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => ClientHomeNavigation()));

        // Navigator.pushNamed(context, "Verify");
      }
    }

  }

  @override
  void dispose() {
    super.dispose();
  }
}
