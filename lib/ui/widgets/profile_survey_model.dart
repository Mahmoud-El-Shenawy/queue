class ProfileSurvey {
  List<ExtraInfo> _extraInfo;

  ProfileSurvey({List<ExtraInfo> extraInfo}) {
    this._extraInfo = extraInfo;
  }

  List<ExtraInfo> get extraInfo => _extraInfo;
  set extraInfo(List<ExtraInfo> extraInfo) => _extraInfo = extraInfo;

  ProfileSurvey.fromJson(Map<String, dynamic> json) {
    if (json['extra_info'] != null) {
      _extraInfo = new List<ExtraInfo>();
      json['extra_info'].forEach((v) {
        _extraInfo.add(new ExtraInfo.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._extraInfo != null) {
      data['extra_info'] = this._extraInfo.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ExtraInfo {
  String _id;
  String _answer;

  ExtraInfo({String id, String answer}) {
    this._id = id;
    this._answer = answer;
  }

  String get id => _id;
  set id(String id) => _id = id;
  String get answer => _answer;
  set answer(String answer) => _answer = answer;

  ExtraInfo.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _answer = json['answer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['answer'] = this._answer;
    return data;
  }
}
