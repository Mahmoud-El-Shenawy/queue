import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:queue/utils/app_localizations.dart';
import 'package:queue/style/theme.dart' as Theme;
import 'file:///C:/Users/Mahmoud/Desktop/QueueOver/queue/lib/ui/screens/sign_in.dart';

class SliderSplash extends StatefulWidget {
  @override
  _SliderSplashState createState() {
    return _SliderSplashState();
  }
}

class _SliderSplashState extends State<SliderSplash> {
  List<SliderModel> mySlides = List<SliderModel>();
  int slideIndex = 0;
  PageController controller;
  var myId;

  Widget _buildPageIndicator(bool isCurrentPage) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 2.0),
      height: isCurrentPage ? 10.0 : 6.0,
      width: isCurrentPage ? 10.0 : 6.0,
      decoration: BoxDecoration(
        color:
            isCurrentPage ? Theme.Colors.darkAccentColor : Theme.Colors.grey300,
        borderRadius: BorderRadius.circular(12),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    mySlides = getSlides();
    controller = new PageController();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
          backgroundColor: Colors.white,
          body: Container(
            child: PageView(
              controller: controller,
              onPageChanged: (index) {
                setState(() {
                  slideIndex = index;
                });
              },
              children: <Widget>[
                SlideTile(
                  imagePath: mySlides[0].getImageAssetPath(),
                  title: mySlides[0].getTitle(),
                  desc: mySlides[0].getDesc(),
                  color: Color(0xFF202334),
                ),
                SlideTile(
                  imagePath: mySlides[1].getImageAssetPath(),
                  title: mySlides[1].getTitle(),
                  desc: mySlides[1].getDesc(),
                  color: Color(0xFF477B72),
                ),
                SlideTile(
                  imagePath: mySlides[2].getImageAssetPath(),
                  title: mySlides[2].getTitle(),
                  desc: mySlides[2].getDesc(),
                  color: Color(0xFF442447),
                ),
                SlideTile(
                  imagePath: mySlides[3].getImageAssetPath(),
                  title: mySlides[3].getTitle(),
                  desc: mySlides[3].getDesc(),
                  color: Color(0xFF007CE3),
                ),
                SlideTile(
                  imagePath: mySlides[4].getImageAssetPath(),
                  title: mySlides[4].getTitle(),
                  desc: mySlides[4].getDesc(),
                  color: Color(0xFFFFB883),
                )
              ],
            ),
          ),
          bottomSheet: slideIndex != 4
              ? Container(
                  color: Colors.white,
                  height: 80,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                  builder: (context) => SignInScreen(myId)));
                        },
                        splashColor: Colors.white,
                        child: Text(
                          AppLocalizations.of(context).translate('skip'),
                          style: TextStyle(
                              color: Colors.grey, fontWeight: FontWeight.w600),
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            for (int i = 0; i < 5; i++)
                              i == slideIndex
                                  ? _buildPageIndicator(true)
                                  : _buildPageIndicator(false),
                          ],
                        ),
                      ),
                      FlatButton(
                        onPressed: () {
                          print("this is slideIndex: $slideIndex");
                          controller.animateToPage(slideIndex + 1,
                              duration: Duration(milliseconds: 500),
                              curve: Curves.linear);
                        },
                        splashColor: Color(0xFF477B72),
                        child: Text(
                          AppLocalizations.of(context).translate('next'),
                          style: TextStyle(
                              color: Color(0xFF477B72),
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ],
                  ),
                )
              : Container(
                  color: Colors.white,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        color: Colors.white,
                        margin: EdgeInsets.only(bottom: 20),
                        child: InkWell(
                            onTap: () {
                              Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                      builder: (context) => SignInScreen(myId)));
                            },
                            child: Container(
                              height: 40,
                              width: 40,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xFF57C3B3),
                              ),
                              child: Icon(
                                Icons.arrow_forward,
                                color: Colors.white,
                              ),
                            )),
                      )
                    ],
                  ),
                )),
    );
  }
}

class SliderModel {
  String imageAssetPath;
  String title;
  String desc;

  SliderModel({this.imageAssetPath, this.title, this.desc});

  void setImageAssetPath(String getImageAssetPath) {
    imageAssetPath = getImageAssetPath;
  }

  void setTitle(String getTitle) {
    title = getTitle;
  }

  void setDesc(String getDesc) {
    desc = getDesc;
  }

  String getImageAssetPath() {
    return imageAssetPath;
  }

  String getTitle() {
    return title;
  }

  String getDesc() {
    return desc;
  }
}

List<SliderModel> getSlides() {
  List<SliderModel> slides = new List<SliderModel>();
  SliderModel sliderModel = new SliderModel();

  //1
  sliderModel
      .setDesc("The App notifies your customers when their turn approach");
  sliderModel.setTitle("Now Serving");
  sliderModel.setImageAssetPath("assets/images/vectorrr.png");
  slides.add(sliderModel);

  sliderModel = SliderModel();

  //2
  sliderModel
      .setDesc("Allowing you to share uptates and receive customer feedback");
  sliderModel.setTitle("Status Uptates");
  sliderModel.setImageAssetPath("assets/images/vectorsend.png");
  slides.add(sliderModel);

  sliderModel = SliderModel();

  //3
  sliderModel.setDesc(
      "Schedule An Appointment or join a virtual queue via smart phone");
  sliderModel.setTitle("Schedule An Appointment");
  sliderModel.setImageAssetPath("assets/images/vectorcl.png");
  slides.add(sliderModel);

  sliderModel = SliderModel();

  //4
  sliderModel.setDesc("Reduces Customer wait  &  Service time by 80% to 90%");
  sliderModel.setTitle("Reduces Wait Time");
  sliderModel.setImageAssetPath("assets/images/vectorr.png");
  slides.add(sliderModel);

  sliderModel = SliderModel();

  //5
  sliderModel.setDesc("");
  sliderModel.setTitle("Eliminates Irregular and haphazard queuing");
  sliderModel.setImageAssetPath("assets/images/vector.png");
  slides.add(sliderModel);

  return slides;
}

class SlideTile extends StatelessWidget {
  final String imagePath, title, desc;
  final Color color;

  SlideTile({this.imagePath, this.title, this.desc, this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Column(
            children: [
              Stack(
                children: [
                  Container(
                    child: ClipRRect(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(30.0),
                          bottomRight: Radius.circular(30.0)),
                      child: Container(
                        height: 350.0,
                        color: color,
                        width: double.infinity,
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      margin: EdgeInsets.only(top: 50),
                      child: Image.asset(
                        imagePath,
                        height: 300,
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
          SizedBox(
            height: 40,
          ),
          Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            desc,
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
          )
        ],
      ),
    );
  }
}
