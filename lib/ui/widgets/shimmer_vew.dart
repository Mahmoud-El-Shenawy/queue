import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:queue/style/theme.dart' as Theme;

class ShimmerView extends StatefulWidget {
  final double height;
  final double width;
  final double radius;

  const ShimmerView({Key key, this.height, this.width, this.radius}) : super(key: key);

  @override
  _ShimmerViewState createState() => _ShimmerViewState();
}

class _ShimmerViewState extends State<ShimmerView> {
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: Theme.Colors.grey200,
      highlightColor: Theme.Colors.white30,
      child: Container(
        height: widget.height,
        width: widget.width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(widget.radius),
            color: Theme.Colors.grey200),
      ),
    );
  }
}
