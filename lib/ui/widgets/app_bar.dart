import 'package:flutter/material.dart';
import 'package:queue/style/theme.dart' as Theme;

PreferredSize shopinAuthAppBar({List<Widget> actions, String title}) {
  return PreferredSize(
    child: Container(
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Theme.Colors.white,
          offset: Offset(0, 2.0),
          blurRadius: 12.0,
        )
      ]),
      child: AppBar(
        elevation: 0.0,
        centerTitle: true,
        title: Text(title),
        backgroundColor: Theme.Colors.mainColor,
        actions: actions,
      ),
    ),
    preferredSize: Size.fromHeight(kToolbarHeight),
  );
}

PreferredSize qGeneralAppBar(
    {List<Widget> actions,
    String title,
    Widget leading,
    bool isWithShadow = true}) {
  return PreferredSize(
    child: Container(
      decoration: !isWithShadow
          ? null
          : BoxDecoration(boxShadow: [
              BoxShadow(
                color: Theme.Colors.grey,
                offset: Offset(0, 2.0),
                blurRadius: 5.0,
              )
            ]),
      child: AppBar(
          elevation: 0.0,
          centerTitle: true,
          title: Text(
            title,
            style: TextStyle(
              color: Theme.Colors.black,
            ),
          ),
          backgroundColor: Theme.Colors.white,
          actions: actions,
          leading: leading,
          iconTheme: IconThemeData(
            color: Theme.Colors.black, //change your color here
          )),
    ),
    preferredSize: Size.fromHeight(kToolbarHeight),
  );
}
