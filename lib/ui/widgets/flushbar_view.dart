import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:queue/style/theme.dart' as Theme;

flushBarView(BuildContext context, String message, bool success) {
  Flushbar(
    icon: success
        ? Icon(
            Icons.check_circle_outline,
            color: Theme.Colors.white,
          )
        : Icon(
            Icons.error_outline,
            color: Theme.Colors.white,
          ),
    shouldIconPulse: success ? false : true,
    backgroundColor:
        success ? Theme.Colors.greenFlusher : Theme.Colors.redFlusher,
    flushbarStyle: FlushbarStyle.GROUNDED,
    flushbarPosition: FlushbarPosition.TOP,
    message: message,
    duration: Duration(seconds: 2),
  )..show(context);
}
