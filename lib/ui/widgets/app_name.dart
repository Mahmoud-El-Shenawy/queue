import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:queue/style/theme.dart' as Theme;

appName() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Text(
        "NO - ",
        style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.bold,
          color: Theme.Colors.white,
        ),
      ),
      Text(
        "Q",
        style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.bold,
          color: Theme.Colors.darkAccentColor,
        ),
      ),
    ],
  );
}
