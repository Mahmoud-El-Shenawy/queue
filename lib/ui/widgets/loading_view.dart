import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

loadingScreen(BuildContext context){
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: (){return null;},
          child: CupertinoTheme( data: CupertinoTheme.of(context).copyWith(brightness: Brightness.dark), child: CupertinoActivityIndicator(), ),
        );
      });
}