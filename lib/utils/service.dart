import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:queue/utils/global.dart';
import 'package:queue/utils/sharedPrefrence.dart';

class APIProvider {
  APIProvider.internal();
  static const Map<String, String> apiHeaders = {
    "Content-Type": "application/json",
    "Accept": "application/json, text/plain, */*",
    "X-Requested-With": "XMLHttpRequest",
  };

  Future<dynamic> get({BuildContext context,@required String subRout, void successResponse(dynamic response), void errorResponse(dynamic response)}) async {
    String lang = 'ar';
    String token = await getToken();

    var headers;
    if (token == null)
      headers = {"lang": lang, ...apiHeaders};
    else
      headers = {"lang": lang, "Authorization": token, ...apiHeaders};

    http.Response response = await http.get( "APIRouting.baseURL+subRout",headers: headers);

    var decoded = json.decode(response.body);

    debugApi(
        statusCode: response.statusCode,
        response: decoded,
        data: null,
        endPoint: response.request.url,
        headers: headers);

    if(response.statusCode == 200){
      successResponse(decoded);
    }
    else if(response.statusCode == 401){
    }
    else{

      errorResponse(decoded);
    }
  }

  Future<dynamic> postMultipartFile({@required String subRout,@required Map<String,String> body,Map<String,String> multiPart,@required void successResponse(dynamic response),@required void errorResponse(dynamic response)}) async {
    print('postMultipartFile');
    http.StreamedResponse response;
    var url = Uri.parse(baseURL);
    String lang = 'ar';
    String token = await getToken();
    var headers;

    if (token == null)
      headers = {"lang": lang, ...apiHeaders};
    else
      headers = {"lang": lang, "Authorization": token, ...apiHeaders};

    var request = http.MultipartRequest('POST', url);

    request.headers.addAll(headers);

    request.fields.addAll(body);
    multiPart.forEach((k,v) async {
      request.files.add(await http.MultipartFile.fromPath(k, v));
    });
    response = await request.send();
    var rBody  = await response.stream.bytesToString();
    var decoded = jsonDecode(rBody);

    debugApi(
        statusCode: response.statusCode,
        response: decoded,
        data: body,
        endPoint: response.request.url,
        headers: headers);

    if(response.statusCode == 200){
      successResponse(decoded);
    }
    else{
      errorResponse(decoded);
    }
  }

  debugApi({String fileName = "APIProvider.dart", @required endPoint, @required int statusCode, @required response, @required data, headers}) {
    debugPrint(
      "FileName: $fileName\n"
          "${endPoint != null ? 'EndPoint: $endPoint\n' : ''}"
          "${data != null ? 'Sent with data: $data\n' : ''}"
          "${headers != null ? "sent with Headerss :$headers" : ""}"
          "Returned with statusCode: $statusCode\n"
          "${response != null ? 'Returned with Response: $response\n' : ''}"
          "--------------------",
      wrapWidth: 512,
    );
  }

  Future<dynamic> post({@required String subRout,@required String body,@required void successResponse(dynamic response),@required void errorResponse(dynamic response)}) async {
    var url = Uri.parse(baseURL);
    String lang = 'ar';
    String token = await getToken();
    var headers;
    if (token == null)
      headers = {"lang": lang, ...apiHeaders};
    else
      headers = {"lang": lang, "Authorization": token, ...apiHeaders};
    http.Response response = await http.post( url ,body: body,headers: headers,encoding: Encoding.getByName("utf-8"));
    var decoded = json.decode(response.body);

    debugApi(
        statusCode: response.statusCode,
        response: decoded,
        data: body,
        endPoint: response.request.url,
        headers: headers);

    if(response.statusCode == 200){
      successResponse(decoded);
    }
    else{
      errorResponse(decoded);
    }
  }

}
