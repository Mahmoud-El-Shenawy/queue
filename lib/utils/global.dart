import 'package:shared_preferences/shared_preferences.dart';

bool loginState = false;
String baseURL = "http://no-q.cropfieldeg.com/api/";
Map profileData = Map();
List imagesList = List();
String selectedMainCategoryID,
    selectedBrandID,
    selectedBrandCategoryID,
    selectedItemID;

List<String> itemIDs = List();
List<String> itemCounts = List();
List<String> itemTitles = List();
List<String> itemImages = List();

void saveItems() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setStringList("itemIDs", itemIDs);
  prefs.setStringList("itemCounts", itemCounts);
  prefs.setStringList("itemTitles", itemTitles);
  prefs.setStringList("itemImages", itemImages);
}

void getSavedItems() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.getStringList("itemIDs") != null
      ? itemIDs = prefs.getStringList("itemIDs")
      : List();
  prefs.getStringList("itemCounts") != null
      ? itemCounts = prefs.getStringList("itemCounts")
      : List();
  prefs.getStringList("itemTitles") != null
      ? itemTitles = prefs.getStringList("itemTitles")
      : List();
  prefs.getStringList("itemImages") != null
      ? itemImages = prefs.getStringList("itemImages")
      : List();
}

void clearBasket() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.remove("itemIDs");
  prefs.remove("itemCounts");
  prefs.remove("itemTitles");
  prefs.remove("itemImages");
}

String fbToken;
String selectedOrderID;
