import 'package:flutter/material.dart';

class Colors {
  const Colors();

  static const Color mainColor = const Color(0xFF202334);
  static const Color accentColor = const Color(0xFF57C3B3);
  static const Color darkAccentColor = const Color(0xFF477B72);
  static const Color white = const Color(0xFFFFFFFF);
  static const Color white30 = const Color(0x4DFFFFFF);
  static const Color white70 = const Color(0xB3FFFFFF);
  static const Color black = const Color(0xFF000000);
  static const Color grey = const Color(0xFF9E9E9E);
  static const Color grey200 = const Color(0xFFEEEEEE);
  static const Color grey300 = const Color(0xFFE0E0E0);
  static const Color grey400 = const Color(0xFFBDBDBD);
  static const Color grey800 = const Color(0xFF424242);
  static const Color grey900 = const Color(0xFF212121);
  static const Color amber = const Color(0xFFFFC107);
  static const Color blueGray = const Color(0xFF607D8B);
  static const Color red = const Color(0xFFF44336);
  static const Color redFlusher = const Color(0xFFc0392b);
  static const Color black87 = const Color(0xDD000000);
  static const Color black54 = const Color(0x8A000000);
  static const Color green = const Color(0xFF4CAF50);
  static const Color greenFlusher = const Color(0xFF27ae60);




}
