class QuestionReq {
  String _id;
  List<Question> _question = new List();

  QuestionReq({String id, List<Question> question}) {
    this._id = id;
    this._question = question;
  }

  String get id => _id;
  set id(String id) => _id = id;
  List<Question> get question => _question;
  set question(List<Question> question) => _question = question;

  QuestionReq.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    if (json['question'] != null) {
      _question = new List<Question>();
      json['question'].forEach((v) {
        _question.add(new Question.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    if (this._question != null) {
      data['question'] = this._question.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Question {
  String _questionId;
  String _answer;

  Question({String questionId, String answer}) {
    this._questionId = questionId;
    this._answer = answer;
  }

  String get questionId => _questionId;
  set questionId(String questionId) => _questionId = questionId;
  String get answer => _answer;
  set answer(String answer) => _answer = answer;

  Question.fromJson(Map<String, dynamic> json) {
    _questionId = json['question_id'];
    _answer = json['answer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['question_id'] = this._questionId;
    data['answer'] = this._answer;
    return data;
  }
}
